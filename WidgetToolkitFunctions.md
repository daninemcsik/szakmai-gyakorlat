# Swing

## Primitives:

* Point2D()
* Line2D()
* Rectangle2d()
* RoundRectangle2D()
* Arc2D()
* Ellipse2D()
* QuadCurve2D()
* CubicCurve2D()

## Interface elements:

* JFrame()
* JPanel()
* JInternalFrame()
* JLayeredPane()
* BufferedImage()


# QT

## Primitives:

* drawPoint()
* drawPoints()
* drawLine()
* drawRect()
* drawRoundedRect()
* drawEllipse
* drawChord
* drawPolygon()
* drawImage()

## Interface elements:

* QMainWindow()
* QWidget()
* QFrame()
* QLayout()
* QLabel()


# WPF

## Primitives: 

* XML <Line> 	|| c# Line()
* XML <Ellipse>	|| c# Ellipse()
* XML <Path> 	|| c# PathGeometry()
* XML <Polygon> 	|| c# Polygon()
* XML <Rectangle> || c# System.Windows.Shapes.Rectangle()

## Interface elements:

* XML <Border> 	|| c# Border()
* XML <Window>	|| c# Window()
* XML <Canvas> 	|| c# Canvas()
* XML <StackPanel>|| c# StackPanel()


# wxWidget:

##Primitives:

* DrawPoint()
* DrawLine
* DrawLines()
* DrawPolygon()
* DrawCircle()
* DrawEllipse()

## Interface elements:

* wxWindow()
* wxFrame()
* wxControl()
* wxPanel()
* wxSizer()


# GTK

## Primitives:

* gdk_draw_point()
* gdk_draw_line()
* gdk_draw_rectangle()
* gdk_draw_arc()
* gdk_draw_image()

## Interface elements:

* gtk_window_new()
* gtk_application_new()
* gtk_application_window_new()
* gtk_overlay_new()
* gtk_label_new()


# Canvas

<!-- const ctx = canvas.getContext('2d') -->

* <canvas></canvas>
* ctx.arc()
* ctx.rect()/fillRect()/strokeRect()
* ctx.moveTo()
* ctx.lineTo()
* ctx.beginPath()
* ctx.stroke()
* ctx.drawImage()

<!-- canvas esetében nem találtam mást, ezért bennehagytam azt ami volt -->






