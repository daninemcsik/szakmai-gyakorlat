#include <stdio.h>
#include <gtk/gtk.h>
#include <math.h>


void activate (GtkApplication* app, gpointer user_data);

void on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data);

int main () {

  GtkApplication *app = gtk_application_new ("Example.app", G_APPLICATION_FLAGS_NONE);
  g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
  g_application_run (G_APPLICATION (app), 0, NULL);
  g_object_unref(app);

  return 0;
}

void on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data) {
    cairo_save(cr);
    cairo_rectangle(cr, 10, 20, 130, 80);
    cairo_set_source_rgb(cr, 1, 0, 0);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_rectangle(cr, 150, 20, 130, 80);
    cairo_set_source_rgb(cr, 0, 1, 0);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_rectangle(cr, 290, 20, 130, 80);
    cairo_set_source_rgb(cr, 0, 0, 1);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_rectangle(cr, 430, 20, 130, 80);
    cairo_set_source_rgb(cr, 1, 1, 0);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_rectangle(cr, 570, 20, 130, 80);
    cairo_set_source_rgb(cr, 1, 0, 1);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_rectangle(cr, 710, 20, 130, 80);
    cairo_set_source_rgb(cr, 0, 1, 1);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_rectangle(cr, 850, 20, 130, 80);
    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
    cairo_restore(cr);




    cairo_save(cr);
    cairo_rectangle(cr, 10, 135, 130, 80);
    cairo_set_source_rgba(cr, 1, 0, 0, 0.7);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_rectangle(cr, 150, 135, 130, 80);
    cairo_set_source_rgba(cr, 0, 1, 0, 0.6);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_rectangle(cr, 290, 135, 130, 80);
    cairo_set_source_rgba(cr, 0, 0, 1, 0.4);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_rectangle(cr, 430, 135, 130, 80);
    cairo_set_source_rgba(cr, 1, 1, 0, 0.2);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_rectangle(cr, 570, 135, 130, 80);
    cairo_set_source_rgba(cr, 1, 0, 1, 0.1);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_rectangle(cr, 710, 135, 130, 80);
    cairo_set_source_rgba(cr, 0, 1, 1, 0.2);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_rectangle(cr, 850, 135, 130, 80);
    cairo_set_source_rgba(cr, 0, 0, 0, 0.5);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
    cairo_restore(cr);



    cairo_save(cr);
    cairo_move_to(cr, 40, 113);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "r g b(1, 0, 0)");
    cairo_restore(cr);

    cairo_save(cr);
    cairo_move_to(cr, 180, 113);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "r g b(0, 1, 0)");
    cairo_restore(cr);

    cairo_save(cr);
    cairo_move_to(cr, 320, 113);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "r g b(0, 0, 1)");
    cairo_restore(cr);

    cairo_save(cr);
    cairo_move_to(cr, 460, 113);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "r g b(1, 1, 0)");
    cairo_restore(cr);

    cairo_save(cr);
    cairo_move_to(cr, 600, 113);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "r g b(1, 0, 1)");
    cairo_restore(cr);

    cairo_save(cr);
    cairo_move_to(cr, 740, 113);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "r g b(0, 1, 1)");
    cairo_restore(cr);

    cairo_save(cr);
    cairo_move_to(cr, 880, 113);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "r g b(0, 0, 0)");
    cairo_restore(cr);








    cairo_save(cr);
    cairo_move_to(cr, 30, 233);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "r g b a(1, 0, 0, 0.7)");
    cairo_restore(cr);

    cairo_save(cr);
    cairo_move_to(cr, 170, 233);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "r g b a(0, 1, 0, 0.6)");
    cairo_restore(cr);

    cairo_save(cr);
    cairo_move_to(cr, 310, 233);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "r g b a(0, 0, 1, 0.4)");
    cairo_restore(cr);

    cairo_save(cr);
    cairo_move_to(cr, 450, 233);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "r g b a(1, 1, 0, 0.2)");
    cairo_restore(cr);

    cairo_save(cr);
    cairo_move_to(cr, 590, 233);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "r g b a(1, 0, 1, 0.1)");
    cairo_restore(cr);

    cairo_save(cr);
    cairo_move_to(cr, 730, 233);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "r g b a(0, 1, 1, 0.2)");
    cairo_restore(cr);

    cairo_save(cr);
    cairo_move_to(cr, 870, 233);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "r g b a(0, 0, 0, 0.5)");
    cairo_restore(cr);


}




void activate (GtkApplication* app, gpointer user_data) {

  GtkWidget *window = gtk_application_window_new(app);
  gtk_window_set_title (GTK_WINDOW (window), "Example");
  GtkWidget *drawing_area = gtk_drawing_area_new ();
  gtk_widget_set_size_request (drawing_area, 1000, 260);
  gtk_container_add(GTK_CONTAINER(window), drawing_area);
  g_signal_connect(G_OBJECT(drawing_area), "draw", G_CALLBACK(on_draw_event), NULL);
  gtk_widget_show_all(window);
}
