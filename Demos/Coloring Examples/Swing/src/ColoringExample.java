import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.*;
import java.awt.geom.Ellipse2D.Double;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class ColoringExample extends JFrame{

	public ColoringExample() {
		setTitle("Example");
		setSize(720,400);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
	}
	
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		
		g2.setPaint(Color.black);
		g2.fill(new Rectangle2D.Double(10, 40, 130, 80));
		
		g2.setPaint(Color.BLUE);
		g2.fill(new Rectangle2D.Double(150, 40, 130, 80));
		
		g2.setPaint(Color.CYAN);
		g2.fill(new Rectangle2D.Double(290, 40, 130, 80));
		
		g2.setPaint(Color.DARK_GRAY);
		g2.fill(new Rectangle2D.Double(430, 40, 130, 80));
		
		g2.setPaint(Color.GRAY);
		g2.fill(new Rectangle2D.Double(570, 40, 130, 80));
		
		
		g2.setPaint(Color.GREEN);
		g2.fill(new Rectangle2D.Double(10, 155, 130, 80));
		
		g2.setPaint(Color.MAGENTA);
		g2.fill(new Rectangle2D.Double(150, 155, 130, 80));
		
		g2.setPaint(Color.ORANGE);
		g2.fill(new Rectangle2D.Double(290, 155, 130, 80));
		
		g2.setPaint(Color.PINK);
		g2.fill(new Rectangle2D.Double(430, 155, 130, 80));
		
		g2.setPaint(Color.RED);
		g2.fill(new Rectangle2D.Double(570, 155, 130, 80));
		
		
		g2.setPaint(new Color(52, 235, 119));
		g2.fill(new Rectangle2D.Double(10, 280, 130, 80));
		
		g2.setPaint(new Color(139, 83, 230));
		g2.fill(new Rectangle2D.Double(150, 280, 130, 80));
		
		g2.setPaint(new Color(145, 137, 51));
		g2.fill(new Rectangle2D.Double(290, 280, 130, 80));
		
		g2.setPaint(new Color(29, 88, 89));
		g2.fill(new Rectangle2D.Double(430, 280, 130, 80));
		
		g2.setPaint(new Color(89, 29, 68));
		g2.fill(new Rectangle2D.Double(570, 280, 130, 80));
		
		g2.setPaint(Color.BLACK);
		
		g2.drawString("black", 60, 130);
		g2.drawString("blue", 200, 130);
		g2.drawString("cyan", 340, 130);
		g2.drawString("dark gray", 470, 130);
		g2.drawString("gray", 620, 130);
		
		g2.drawString("green", 60, 250);
		g2.drawString("magenta", 190, 250);
		g2.drawString("orange", 340, 250);
		g2.drawString("pink", 480, 250);
		g2.drawString("red", 620, 250);
		
		g2.drawString("#34eb77", 50, 370);
		g2.drawString("#8b53e6", 190, 370);
		g2.drawString("#918933", 330, 370);
		g2.drawString("#1d5859", 470, 370);
		g2.drawString("#591d44", 610, 370);
		
		
		g2.setPaint(Color.BLACK);
		g2.setStroke(new BasicStroke(2));
		g2.drawLine(0, 270,  710, 270);
		
	}

	
	
	public static void main(String[] args) {
		ColoringExample e = new ColoringExample();
	}
}
