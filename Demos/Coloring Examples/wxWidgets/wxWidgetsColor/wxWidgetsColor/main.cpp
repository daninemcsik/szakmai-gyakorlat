#include "wx/wx.h"
#include "wx/sizer.h"

class BasicDrawPane : public wxPanel
{

public:
    BasicDrawPane(wxFrame* parent);

    void paintEvent(wxPaintEvent& evt);
    void paintNow();

    void render(wxDC& dc);

    DECLARE_EVENT_TABLE()
};

class MyApp : public wxApp
{
    bool OnInit();

    wxFrame* frame;
    BasicDrawPane* drawPane;
public:

};

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
    wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
    frame = new wxFrame((wxFrame*)NULL, -1, wxT("Hello wxDC"), wxPoint(50, 50), wxSize(750, 430));

    drawPane = new BasicDrawPane((wxFrame*)frame);
    sizer->Add(drawPane, 1, wxEXPAND);

    frame->SetSizer(sizer);
    frame->SetAutoLayout(true);
    drawPane->SetBackgroundColour(*wxWHITE);
    frame->Show();
    return true;
}

BEGIN_EVENT_TABLE(BasicDrawPane, wxPanel)

EVT_PAINT(BasicDrawPane::paintEvent)

END_EVENT_TABLE()

BasicDrawPane::BasicDrawPane(wxFrame* parent) : wxPanel(parent) {}

void BasicDrawPane::paintEvent(wxPaintEvent& evt)
{
    wxPaintDC dc(this);
    render(dc);
}


void BasicDrawPane::paintNow()
{
    wxClientDC dc(this);
    render(dc);
}

void BasicDrawPane::render(wxDC& dc)
{
    wxPen pen(*wxBLACK, 2, wxPENSTYLE_SOLID);
    dc.SetPen(pen);
    wxBrush brush(*wxBLACK, wxBRUSHSTYLE_SOLID);
    dc.SetBrush(brush);
    dc.DrawRectangle(10, 20, 130, 80);

    brush.SetColour(*wxBLUE);
    dc.SetBrush(brush);
    dc.DrawRectangle(150, 20, 130, 80);

    brush.SetColour(*wxCYAN);
    dc.SetBrush(brush);
    dc.DrawRectangle(290, 20, 130, 80);

    brush.SetColour(*wxGREEN);
    dc.SetBrush(brush);
    dc.DrawRectangle(430, 20, 130, 80);

    brush.SetColour(*wxYELLOW);
    dc.SetBrush(brush);
    dc.DrawRectangle(570, 20, 130, 80);

    brush.SetColour(*wxLIGHT_GREY);
    dc.SetBrush(brush);
    dc.DrawRectangle(10, 135, 130, 80);

    brush.SetColour(*wxRED);
    dc.SetBrush(brush);
    dc.DrawRectangle(150, 135, 130, 80);

    brush.SetColour(*wxWHITE);
    dc.SetBrush(brush);
    dc.DrawRectangle(290, 135, 130, 80);

    dc.DrawLine(0, 250, 750, 250);

    brush.SetColour(wxColour(52, 235, 119)); 
    dc.SetBrush(brush);
    dc.DrawRectangle(10, 280, 130, 80);

    brush.SetColour(wxColour(139, 83, 230));
    dc.SetBrush(brush);
    dc.DrawRectangle(150, 280, 130, 80);

    brush.SetColour(wxColour(145, 137, 51));
    dc.SetBrush(brush);
    dc.DrawRectangle(290, 280, 130, 80);

    brush.SetColour(wxColour(29, 88, 89));
    dc.SetBrush(brush);
    dc.DrawRectangle(430, 280, 130, 80);

    brush.SetColour(wxColour(89, 29, 68));
    dc.SetBrush(brush);
    dc.DrawRectangle(570, 280, 130, 80);
    

    dc.DrawText("black", 60, 100);
    dc.DrawText("blue", 200, 100);
    dc.DrawText("cyan", 340, 100);
    dc.DrawText("green", 480, 100);
    dc.DrawText("yellow", 620, 100);

    dc.DrawText("light grey", 50, 220);
    dc.DrawText("red", 200, 220);
    dc.DrawText("white", 350, 220);

    dc.DrawText("#34eb77", 60, 360);
    dc.DrawText("#8b53e6", 200, 360);
    dc.DrawText("#918933", 340, 360);
    dc.DrawText("#1d5859", 480, 360);
    dc.DrawText("#591d44", 620, 360);

}