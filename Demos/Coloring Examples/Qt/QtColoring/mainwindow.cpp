#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter obj(this);


    QBrush brush(Qt::black, Qt::SolidPattern);
    obj.setBrush(brush);
    obj.drawRect(10, 20, 130, 80);

    brush.setColor(Qt::cyan);
    obj.setBrush(brush);
    obj.drawRect(150, 20, 130, 80);

    brush.setColor(Qt::red);
    obj.setBrush(brush);
    obj.drawRect(290, 20, 130, 80);

    brush.setColor(Qt::magenta);
    obj.setBrush(brush);
    obj.drawRect(430, 20, 130, 80);

    brush.setColor(Qt::darkGray);
    obj.setBrush(brush);
    obj.drawRect(570, 20, 130, 80);


    brush.setColor(Qt::green);
    obj.setBrush(brush);
    obj.drawRect(10, 135, 130, 80);

    brush.setColor(Qt::blue);
    obj.setBrush(brush);
    obj.drawRect(150, 135, 130, 80);

    brush.setColor(Qt::darkCyan);
    obj.setBrush(brush);
    obj.drawRect(290, 135, 130, 80);

    brush.setColor(Qt::lightGray);
    obj.setBrush(brush);
    obj.drawRect(430, 135, 130, 80);

    brush.setColor(Qt::darkBlue);
    obj.setBrush(brush);
    obj.drawRect(570, 135, 130, 80);

    obj.drawLine(0, 250, 750, 250);

    brush.setColor(QColor(52, 235, 119));
    obj.setBrush(brush);
    obj.drawRect(10, 280, 130, 80);

    brush.setColor(QColor(139, 83, 230));
    obj.setBrush(brush);
    obj.drawRect(150, 280, 130, 80);

    brush.setColor(QColor(145, 137, 51));
    obj.setBrush(brush);
    obj.drawRect(290, 280, 130, 80);

    brush.setColor(QColor(29, 88, 89));
    obj.setBrush(brush);
    obj.drawRect(430, 280, 130, 80);

    brush.setColor(QColor(89, 29, 68));
    obj.setBrush(brush);
    obj.drawRect(570, 280, 130, 80);


    obj.drawText(60, 113, "black");
    obj.drawText(200, 113, "cyan");
    obj.drawText(350, 113, "red");
    obj.drawText(470, 113, "magenta");
    obj.drawText(610, 113, "darkGray");

    obj.drawText(60, 230, "green");
    obj.drawText(200, 230, "blue");
    obj.drawText(330, 230, "darkCyan");
    obj.drawText(470, 230, "lightGray");
    obj.drawText(610, 230, "darkBlue");

    obj.drawText(50, 380, "#34eb77");
    obj.drawText(190, 380, "#8b53e6");
    obj.drawText(340, 380, "#918933");
    obj.drawText(480, 380, "#1d5859");
    obj.drawText(620, 380, "#591d44");


}
