#include "wx/wx.h"
#include "wx/sizer.h"

class BasicDrawPane : public wxPanel
{

public:
    BasicDrawPane(wxFrame* parent);

    void paintEvent(wxPaintEvent& evt);
    void paintNow();

    void render(wxDC& dc);

    DECLARE_EVENT_TABLE()
};

class MyApp : public wxApp
{
    bool OnInit();

    wxFrame* frame;
    BasicDrawPane* drawPane;
public:

};

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
    wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
    frame = new wxFrame((wxFrame*)NULL, -1, wxT("Hello wxDC"), wxPoint(50, 50), wxSize(800, 280));

    drawPane = new BasicDrawPane((wxFrame*)frame);
    sizer->Add(drawPane, 1, wxEXPAND);

    frame->SetSizer(sizer);
    frame->SetAutoLayout(true);
    drawPane->SetBackgroundColour(*wxWHITE);
    frame->Show();
    return true;
}

BEGIN_EVENT_TABLE(BasicDrawPane, wxPanel)

EVT_PAINT(BasicDrawPane::paintEvent)

END_EVENT_TABLE()

BasicDrawPane::BasicDrawPane(wxFrame* parent) : wxPanel(parent) {}

void BasicDrawPane::paintEvent(wxPaintEvent& evt)
{
    wxPaintDC dc(this);
    render(dc);
}


void BasicDrawPane::paintNow()
{
    wxClientDC dc(this);
    render(dc);
}

void BasicDrawPane::render(wxDC& dc)
{
    wxAffineMatrix2D d;

    dc.DrawRectangle(20, 60, 70, 70);
    dc.DrawRectangle(270, 60, 70, 70);
    dc.DrawRectangle(570, 60, 70, 70);


    dc.DrawLine(250, 0, 250, 400);
    dc.DrawLine(560, 0, 560, 400);
    
    dc.DrawText("Rotate", 100, 20);
    dc.DrawText("Scale", 370, 20);
    dc.DrawText("Translate", 650, 20);

    d.Rotate(0.5);
    dc.SetTransformMatrix(d);
    dc.DrawRectangle(170, -40, 70, 70);

    d.Rotate(-0.5);
    d.Scale(2, 2);
    dc.SetTransformMatrix(d);
    dc.DrawRectangle(200, 30, 70, 70);

    d.Scale(0.5, 0.5);
    d.Translate(100, 40);
    dc.SetTransformMatrix(d);
    dc.DrawRectangle(570, 60, 70, 70);
}