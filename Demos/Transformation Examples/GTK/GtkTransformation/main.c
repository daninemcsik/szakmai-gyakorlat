#include <stdio.h>
#include <gtk/gtk.h>
#include <math.h>


void activate (GtkApplication* app, gpointer user_data);

void on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data);

int main () {

  GtkApplication *app = gtk_application_new ("Example.app", G_APPLICATION_FLAGS_NONE);
  g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
  g_application_run (G_APPLICATION (app), 0, NULL);
  g_object_unref(app);

  return 0;
}

void on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data) {

    cairo_save(cr);
    cairo_rectangle(cr, 10, 80, 70, 70);
    cairo_stroke(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_rotate(cr, 0.5);
    cairo_translate(cr, 10, -90);
    cairo_rectangle(cr, 150, 80, 70, 70);
    cairo_stroke(cr);
    cairo_restore(cr);

    cairo_move_to(cr, 220, 0);
    cairo_line_to(cr, 220, 400);
    cairo_stroke(cr);


    cairo_save(cr);
    cairo_rectangle(cr, 240, 80, 70, 70);
    cairo_stroke(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_scale(cr, 2, 2);
    cairo_translate(cr, -100, -20);
    cairo_rectangle(cr, 280, 40, 70, 70);
    cairo_stroke(cr);
    cairo_restore(cr);

    cairo_move_to(cr, 520, 0);
    cairo_line_to(cr, 520, 400);
    cairo_stroke(cr);


    cairo_save(cr);
    cairo_rectangle(cr, 540, 80, 70, 70);
    cairo_stroke(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_translate(cr, 50, 50);
    cairo_rectangle(cr, 540, 80, 70, 70);
    cairo_stroke(cr);
    cairo_restore(cr);



    cairo_save(cr);
    cairo_move_to(cr, 300, 20);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "Scale Transform");
    cairo_restore(cr);

    cairo_save(cr);
    cairo_move_to(cr, 50, 20);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "Rotate Transform");
    cairo_restore(cr);

    cairo_save(cr);
    cairo_move_to(cr, 560, 20);
    cairo_scale(cr, 1.2, 1.2);
    cairo_show_text(cr, "Translate Transfrom");
    cairo_restore(cr);







}




void activate (GtkApplication* app, gpointer user_data) {

  GtkWidget *window = gtk_application_window_new(app);
  gtk_window_set_title (GTK_WINDOW (window), "Example");
  GtkWidget *drawing_area = gtk_drawing_area_new ();
  gtk_widget_set_size_request (drawing_area, 720, 240);
  gtk_container_add(GTK_CONTAINER(window), drawing_area);
  g_signal_connect(G_OBJECT(drawing_area), "draw", G_CALLBACK(on_draw_event), NULL);
  gtk_widget_show_all(window);
}
