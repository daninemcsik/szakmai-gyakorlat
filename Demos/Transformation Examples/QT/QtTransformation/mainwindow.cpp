#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter obj(this);
    obj.drawRect(20,80,70,70);

    obj.save();
    obj.translate(120, -50);
    obj.rotate(40);
    obj.drawRect(120,70,70,70);
    obj.restore();

    obj.drawLine(250, 0, 250, 400);

    obj.drawRect(300,80,70,70);

    obj.save();
    obj.scale(2, 2);
    obj.drawRect(200, 25, 70, 70);
    obj.restore();

    obj.drawLine(570, 0, 570, 400);

    obj.drawRect(600,80,70,70);

    obj.save();
    obj.translate(50, 50);
    obj.drawRect(600, 80, 70, 70);
    obj.save();

    obj.drawText(30, -15, "Rotate Transform");
    obj.drawText(310, -15, "Scale Transform");
    obj.drawText(580, -15, "Translate Transform");

}


