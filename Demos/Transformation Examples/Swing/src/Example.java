import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.geom.*;
import java.awt.Color;

import javax.swing.JFrame;



@SuppressWarnings("serial")
public class Example extends JFrame{

	Example() {
		setTitle("Example");
		setSize(800,300);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
	}
	
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.drawString("Rotate Transform", 80, 60);
		g2.drawString("Scale Transform", 340, 60);
		g2.drawString("Translate Transform", 600, 60);
		
		AffineTransform saveForm = g2.getTransform();

		g2.drawRect(20, 100, 70, 70);
		
		g2.rotate(0.5);
		g2.translate(70, -100);
		g2.drawRect(120, 100, 70, 70);
		g2.setTransform(saveForm);
		
		g2.drawLine(250, 0, 250, 400);
		
		g2.drawRect(270, 100, 70, 70);
		g2.scale(2, 2);
		g2.drawRect(185, 50, 70, 70);
		g2.setTransform(saveForm);
		
		g2.drawLine(530, 0, 530, 400);
		
		g2.drawRect(550, 100, 70, 70);
		
		g2.translate(100, 160);
		g2.drawRect(550, 10, 70, 70);
		g2.setTransform(saveForm);
		
	}

	
	
	public static void main(String[] args)
	{
		Example e =new Example();
	}
}
