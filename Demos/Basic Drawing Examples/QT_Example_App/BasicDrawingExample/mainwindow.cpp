#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter obj(this);
    obj.drawLine(100, 20, 20, 60);
    obj.drawLine(130, 10, 70, 90);
    obj.drawLine(20, 70, 70, 120);

    obj.drawEllipse(25, 175, 100, 200);
    obj.drawEllipse(150, 250, 200, 100);
    obj.drawEllipse(375, 250, 100, 100);

    //Cant draw curved lines directly with any function.

    obj.drawRect(200,50,100,50);
    obj.drawRect(310,40,100,160);

    QPointF points1[4] = {
        QPointF(500,300),
        QPointF(525,350),
        QPointF(550,300),
        QPointF(525,250),
    };
    obj.drawPolygon(points1, 4);


    QPointF points2[4] = {
        QPointF(650, 250),
        QPointF(600,350),
        QPointF(650, 350),
        QPointF(600,250),
    };
    obj.drawPolygon(points2, 4);

    QPointF points3[3] = {
        QPointF(700,300),
        QPointF(750,350),
        QPointF(750,250),
    };
    obj.drawPolygon(points3, 3);

    QRectF rectangle(510.0, 20.0, 80.0, 60.0);
    int startAngle = 30 * 16;
    int spanAngle = 120 * 16;

    QPainter painter(this);
    painter.drawArc(rectangle, startAngle, spanAngle);


    QRectF rectangle2(610.0, 70.0, 180.0, 180.0);
    int startAngle2 = 30 * 24;
    int spanAngle2 = 120 * 24;

    QPainter painter2(this);
    painter.drawArc(rectangle2, startAngle2, spanAngle2);


    QRectF rectangle3(450.0, 55.0, 90.0, 200.0);
    int startAngle3 = 30 * 24;
    int spanAngle3 = 120 * 24;

    QPainter painter3(this);
    painter.drawArc(rectangle3, startAngle3, spanAngle3);
}


