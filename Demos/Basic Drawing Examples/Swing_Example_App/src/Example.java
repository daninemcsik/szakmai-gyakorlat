import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.*;
import java.awt.geom.Ellipse2D.Double;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class Example extends JFrame{

	public Example() {
		setTitle("Example");
		setSize(800,400);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
	}
	
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.drawLine(100, 50, 20, 90);
		g2.drawLine(130, 40, 70, 120);
		g2.drawLine(20, 100, 70, 150);
		
		g2.draw(new Ellipse2D.Double(25, 175, 100, 200));
		g2.draw(new Ellipse2D.Double(150, 250, 200, 100));
		g2.draw(new Ellipse2D.Double(375, 250, 100, 100));
		
		QuadCurve2D q = new QuadCurve2D.Float();
		q.setCurve(150, 200, 200, 270, 350, 200);
		g2.draw(q);
		q.setCurve(200, 75, 130, 175, 300, 50);
		g2.draw(q);
		CubicCurve2D c = new CubicCurve2D.Double();
		c.setCurve(150, 150, 200, 100, 300, 200, 350, 150);
		g2.draw(c);
		
		g2.draw(new Rectangle2D.Double(400, 50, 100, 50));
		g2.draw(new Rectangle2D.Double(510, 40, 100, 160));
		
		int[] quadx = {500, 525, 550, 525};
		int[] quady = {300, 350, 300, 250};
		g2.drawPolygon(quadx, quady, 4);
		
		int[] quadx2 = {650, 600, 650, 600};
		int[] quady2 = {250, 350, 350, 250};
		g2.drawPolygon(quadx2, quady2, 4);
		
		int[] trix = {700, 750, 750};
		int[] triy = {300, 350, 250};
		g2.drawPolygon(trix, triy, 3);
		
		g2.drawArc(650, 50, 70, 70, 0, 270);
		g2.drawArc(650, 150, 70, 70, 90, 180);
		g2.drawArc(700, 100, 70, 70, 200, 200);
	}

	
	
	public static void main(String[] args) {
		Example e = new Example();
	}
}
