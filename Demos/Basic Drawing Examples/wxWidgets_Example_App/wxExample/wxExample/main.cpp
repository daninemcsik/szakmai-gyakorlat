#include "wx/wx.h"
#include "wx/sizer.h"

class BasicDrawPane : public wxPanel
{

public:
    BasicDrawPane(wxFrame* parent);

    void paintEvent(wxPaintEvent& evt);
    void paintNow();

    void render(wxDC& dc);

    DECLARE_EVENT_TABLE()
};

class MyApp : public wxApp
{
    bool OnInit();

    wxFrame* frame;
    BasicDrawPane* drawPane;
public:

};

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
    wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
    frame = new wxFrame((wxFrame*)NULL, -1, wxT("Hello wxDC"), wxPoint(50, 50), wxSize(800, 400));

    drawPane = new BasicDrawPane((wxFrame*)frame);
    sizer->Add(drawPane, 1, wxEXPAND);

    frame->SetSizer(sizer);
    frame->SetAutoLayout(true);
    drawPane->SetBackgroundColour(* wxWHITE);
    frame->Show();
    return true;
}

BEGIN_EVENT_TABLE(BasicDrawPane, wxPanel)

EVT_PAINT(BasicDrawPane::paintEvent)

END_EVENT_TABLE()

BasicDrawPane::BasicDrawPane(wxFrame* parent) : wxPanel(parent){}

void BasicDrawPane::paintEvent(wxPaintEvent& evt)
{
    wxPaintDC dc(this);
    render(dc);
}


void BasicDrawPane::paintNow()
{
    wxClientDC dc(this);
    render(dc);
}

void BasicDrawPane::render(wxDC& dc)
{
    dc.DrawLine(100, 20, 20, 60);
    dc.DrawLine(130, 10, 70, 90);
    dc.DrawLine(20, 70, 70, 120);

    dc.DrawEllipse(25, 150, 100, 200);
    dc.DrawEllipse(150, 250, 200, 100);
    dc.DrawEllipse(375, 250, 100, 100);

    dc.DrawSpline(150, 200, 270, 245, 350, 200);
    dc.DrawSpline(200, 25, 150, 75, 275, 25);
    wxPoint p1[6] = {
        wxPoint(200, 200),
        wxPoint(225, 100),
        wxPoint(250, 200),
        wxPoint(275, 100),
        wxPoint(290, 80),
        wxPoint(300, 100),
    };
    dc.DrawSpline(6, p1);
    
    dc.DrawRectangle(400, 50, 100, 50);
    dc.DrawRectangle(510, 40, 100, 160);

    wxPoint p2[4] = {
        wxPoint(500, 300),
        wxPoint(525, 350),
        wxPoint(550, 300),
        wxPoint(525, 250),
    };
    dc.DrawPolygon(4, p2);

    wxPoint p3[4] = {
        wxPoint(650, 250),
        wxPoint(600, 350),
        wxPoint(650, 350),
        wxPoint(600, 250),
    };
    dc.DrawPolygon(4, p3);

    wxPoint p4[3] = {
        wxPoint(700, 300),
        wxPoint(750, 350),
        wxPoint(750, 250),
    };
    dc.DrawPolygon(3, p4);
}