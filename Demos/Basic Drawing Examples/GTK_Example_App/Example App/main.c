#include <stdio.h>
#include <gtk/gtk.h>
#include <math.h>


void activate (GtkApplication* app, gpointer user_data);

void on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data);

int main () {

  GtkApplication *app = gtk_application_new ("Example.app", G_APPLICATION_FLAGS_NONE);
  g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
  g_application_run (G_APPLICATION (app), 0, NULL);
  g_object_unref(app);

  return 0;
}

void on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data) {
    cairo_set_line_width(cr, 1.0);


    cairo_move_to(cr, 100, 20);
    cairo_line_to(cr, 20, 60);
    cairo_stroke(cr);

    cairo_move_to(cr, 130, 10);
    cairo_line_to(cr, 70, 90);
    cairo_stroke(cr);

    cairo_move_to(cr, 20, 90);
    cairo_line_to(cr, 90, 120);
    cairo_stroke(cr);

    cairo_save(cr);
    cairo_scale(cr, 0.5, 1);
    cairo_arc(cr, 150, 275, 100, 0, 2*M_PI);
    cairo_stroke(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_scale(cr, 1, 0.6);
    cairo_arc(cr, 250, 500, 100, 0, 2*M_PI);
    cairo_stroke(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_scale(cr, 1, 1);
    cairo_arc(cr, 420, 300, 50, 0, 2*M_PI),
    cairo_stroke(cr);
    cairo_restore(cr);

    cairo_move_to(cr, 200, 100);
    cairo_line_to(cr, 250, 100);
    cairo_curve_to(cr, 310, 100, 320, 120, 330, 170);
    cairo_curve_to(cr, 335, 180, 345, 180, 350, 170);
    cairo_curve_to(cr, 350, 170, 380, 135, 290, 50);
    cairo_stroke(cr);

    cairo_move_to(cr, 400, 50);
    cairo_line_to(cr, 400, 100);
    cairo_curve_to(cr, 400, 140, 420, 150, 450, 160);
    cairo_curve_to(cr, 450, 164, 485, 150, 500, 220);
    //cairo_curve_to(cr, 440, 200, 420, 200, 400, 200);
    cairo_stroke(cr);


    cairo_rectangle(cr, 620, 100, 50, 90);
    cairo_stroke(cr);

    cairo_rectangle(cr, 570, 20, 160, 50);
    cairo_stroke(cr);

    cairo_move_to(cr, 620, 390);
    cairo_line_to(cr, 650, 350);
    cairo_line_to(cr, 680, 390);
    cairo_line_to(cr, 620, 390);
    cairo_stroke(cr);

    cairo_move_to(cr, 520, 240);
    cairo_line_to(cr, 580, 370);
    cairo_line_to(cr, 530, 390);
    cairo_line_to(cr, 470, 350);
    cairo_line_to(cr, 620, 260);
    cairo_line_to(cr, 520, 240);
    cairo_stroke(cr);

    cairo_move_to(cr, 700, 250);
    cairo_line_to(cr, 780, 250);
    cairo_line_to(cr, 700, 350);
    cairo_line_to(cr, 680, 300);
    cairo_line_to(cr, 620, 270);
    cairo_line_to(cr, 700, 250);
    cairo_stroke(cr);

    cairo_arc(cr, 190, 200, 40, M_PI/4, M_PI);
    cairo_stroke(cr);

    cairo_arc(cr, 250, 200, 40, M_PI, M_PI/4);
    cairo_stroke(cr);

    cairo_arc(cr, 150, 140, 40, M_PI/2, M_PI/4);
    cairo_stroke(cr);
}


void activate (GtkApplication* app, gpointer user_data) {

  GtkWidget *window = gtk_application_window_new(app);
  gtk_window_set_title (GTK_WINDOW (window), "Example");
  GtkWidget *drawing_area = gtk_drawing_area_new ();
  gtk_widget_set_size_request (drawing_area, 800, 400);
  gtk_container_add(GTK_CONTAINER(window), drawing_area);
  g_signal_connect(G_OBJECT(drawing_area), "draw", G_CALLBACK(on_draw_event), NULL);
  gtk_widget_show_all(window);
}
