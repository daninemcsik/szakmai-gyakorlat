#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter obj(this);


    obj.setFont(QFont("Times", 10, false));
    obj.drawText(10, 20, "Font: Times, characterset: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789áÁéÉíÍóÓöÖőŐúÚüÜűŰ,?.:-_Ł$ß÷×˝¨¸¤( )=/%!+< >@{ }[ ]");

    obj.setFont(QFont("Times", 12, false));
    obj.drawText(10, 50, "Times size 12");

    obj.setFont(QFont("Times", 14, false));
    obj.drawText(10, 80, "Times size 14");

    obj.setFont(QFont("Times", 16, false));
    obj.drawText(10, 110, "Times size 16");

    obj.setFont(QFont("Times", 18, false));
    obj.drawText(10, 140, "Times size 18");



}
