import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.geom.*;
import java.awt.Color;

import javax.swing.JFrame;



@SuppressWarnings("serial")
public class Example extends JFrame{

	Example() {
		setTitle("Example");
		setSize(1600,400);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
	}
	
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		
		g2.setFont(new Font("MONOSPACED", Font.PLAIN, 16));
		g2.drawString("Font: Monospaced, characterset: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
				+ "0123456789������������������,?.:-_�$��׽���( )=/%!+< >@{ }[ ]", 10,50);
		
		
		g2.setFont(new Font("Dialog", Font.PLAIN, 16));
		g2.drawString("Font: Dialog, characterset: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
				+ "0123456789������������������,?.:-_�$��׽���( )=/%!+< >@{ }[ ]", 10,80);
		
		g2.setFont(new Font("Serif", Font.PLAIN, 16));
		g2.drawString("Font: Serif, characterset: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
				+ "0123456789������������������,?.:-_�$��׽���( )=/%!+< >@{ }[ ]", 10,110);
		
		g2.setFont(new Font("Courier New", Font.PLAIN, 16));
		g2.drawString("Font: Courier New, characterset: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
				+ "0123456789������������������,?.:-_�$��׽���( )=/%!+< >@{ }[ ]", 10,140);
		
		g2.setFont(new Font("MONOSPACED", Font.PLAIN, 16));
		g2.drawString("Monospaced size 16", 20, 180);
		
		g2.setFont(new Font("MONOSPACED", Font.PLAIN, 20));
		g2.drawString("Monospaced size 20", 20, 210);
		
		g2.setFont(new Font("MONOSPACED", Font.PLAIN, 24));
		g2.drawString("Monospaced size 24", 20, 240);
		
		g2.setFont(new Font("MONOSPACED", Font.PLAIN, 28));
		g2.drawString("Monospaced size 28", 20, 270);
		
		
		g2.setFont(new Font("Dialog", Font.PLAIN, 16));
		g2.drawString("Dialog size 16", 370, 180);
		
		g2.setFont(new Font("Dialog", Font.PLAIN, 20));
		g2.drawString("Dialog size 20", 370, 210);
		
		g2.setFont(new Font("Dialog", Font.PLAIN, 24));
		g2.drawString("Dialog size 24", 370, 240);
		
		g2.setFont(new Font("Dialog", Font.PLAIN, 28));
		g2.drawString("Dialog size 28", 370, 270);
		
		
		
		g2.setFont(new Font("Courier New", Font.PLAIN, 16));
		g2.drawString("Courier New size 16", 970, 180);
		
		g2.setFont(new Font("Courier New", Font.PLAIN, 20));
		g2.drawString("Courier New size 20", 970, 210);
		
		g2.setFont(new Font("Courier New", Font.PLAIN, 24));
		g2.drawString("Courier New size 24", 970, 240);
		
		g2.setFont(new Font("Courier New", Font.PLAIN, 28));
		g2.drawString("Courier New size 28", 970, 270);
		
		
		
		g2.setFont(new Font("Serif", Font.PLAIN, 16));
		g2.drawString("Serif size 16", 720, 180);
		
		g2.setFont(new Font("Serif", Font.PLAIN, 20));
		g2.drawString("Serif size 20", 720, 210);
		
		g2.setFont(new Font("Serif", Font.PLAIN, 24));
		g2.drawString("Serif size 24", 720, 240);
		
		g2.setFont(new Font("Serif", Font.PLAIN, 28));
		g2.drawString("Serif size 28", 720, 270);
		
	}

	
	
	public static void main(String[] args)
	{
		Example e =new Example();
	}
}
