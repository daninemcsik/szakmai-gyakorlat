#include <stdio.h>
#include <gtk/gtk.h>
#include <math.h>


void activate (GtkApplication* app, gpointer user_data);

void on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data);

int main () {

  GtkApplication *app = gtk_application_new ("Example.app", G_APPLICATION_FLAGS_NONE);
  g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
  g_application_run (G_APPLICATION (app), 0, NULL);
  g_object_unref(app);

  return 0;
}

void on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data) {
    cairo_select_font_face(cr, "Kiro", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size(cr, 12);
    cairo_move_to(cr, 10, 20);
    cairo_show_text(cr, "Font: Kiro, characterset: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789,?.:-$( )=/%!+< >@{ }[ ]");

    cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
    cairo_move_to(cr, 10, 40);
    cairo_show_text(cr, "Font: Courier, characterset: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789,?.:-$( )=/%!+< >@{ }[ ]");

    cairo_select_font_face(cr, "Times New Roman", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
    cairo_move_to(cr, 10, 60);
    cairo_show_text(cr, "Font: Times New Roman, characterset: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789,?.:-$( )=/%!+< >@{ }[ ]");

    cairo_select_font_face(cr, "Verdana", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
    cairo_move_to(cr, 10, 80);
    cairo_show_text(cr, "Font: Verdana, characterset: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789,?.:-$( )=/%!+< >@{ }[ ]");

    cairo_set_font_size(cr,14);
    cairo_move_to(cr, 20, 120);
    cairo_show_text(cr, "Kiro size 14");
    cairo_move_to(cr, 200, 120);
    cairo_show_text(cr, "Courier size 14");
    cairo_move_to(cr, 400, 120);
    cairo_show_text(cr, "Times New Roman size 14");
    cairo_move_to(cr, 700, 120);
    cairo_show_text(cr, "Verdana size 14");


    cairo_set_font_size(cr,16);
    cairo_move_to(cr, 20, 150);
    cairo_show_text(cr, "Kiro size 16");
    cairo_move_to(cr, 200, 150);
    cairo_show_text(cr, "Courier size 16");
    cairo_move_to(cr, 400, 150);
    cairo_show_text(cr, "Times New Roman size 16");
    cairo_move_to(cr, 700, 150);
    cairo_show_text(cr, "Verdana size 16");

    cairo_set_font_size(cr,18);
    cairo_move_to(cr, 20, 180);
    cairo_show_text(cr, "Kiro size 18");
    cairo_move_to(cr, 200, 180);
    cairo_show_text(cr, "Courier size 18");
    cairo_move_to(cr, 400, 180);
    cairo_show_text(cr, "Times New Roman size 18");
    cairo_move_to(cr, 700, 180);
    cairo_show_text(cr, "Verdana size 18");

    cairo_set_font_size(cr,20);
    cairo_move_to(cr, 20, 210);
    cairo_show_text(cr, "Kiro size 20");
    cairo_move_to(cr, 200, 210);
    cairo_show_text(cr, "Courier size 20");
    cairo_move_to(cr, 400, 210);
    cairo_show_text(cr, "Times New Roman size 20");
    cairo_move_to(cr, 700, 210);
    cairo_show_text(cr, "Verdana size 20");
}




void activate (GtkApplication* app, gpointer user_data) {

  GtkWidget *window = gtk_application_window_new(app);
  gtk_window_set_title (GTK_WINDOW (window), "Example");
  GtkWidget *drawing_area = gtk_drawing_area_new ();
  gtk_widget_set_size_request (drawing_area, 1000, 240);
  gtk_container_add(GTK_CONTAINER(window), drawing_area);
  g_signal_connect(G_OBJECT(drawing_area), "draw", G_CALLBACK(on_draw_event), NULL);
  gtk_widget_show_all(window);
}
