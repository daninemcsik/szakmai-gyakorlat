#include "wx/wx.h"
#include "wx/sizer.h"

class BasicDrawPane : public wxPanel
{

public:
    BasicDrawPane(wxFrame* parent);

    void paintEvent(wxPaintEvent& evt);
    void paintNow();

    void render(wxDC& dc);

    DECLARE_EVENT_TABLE()
};

class MyApp : public wxApp
{
    bool OnInit();

    wxFrame* frame;
    BasicDrawPane* drawPane;
public:

};

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
    wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
    frame = new wxFrame((wxFrame*)NULL, -1, wxT("Hello wxDC"), wxPoint(50, 50), wxSize(1900, 430));

    drawPane = new BasicDrawPane((wxFrame*)frame);
    sizer->Add(drawPane, 1, wxEXPAND);

    frame->SetSizer(sizer);
    frame->SetAutoLayout(true);
    drawPane->SetBackgroundColour(*wxWHITE);
    frame->Show();
    return true;
}

BEGIN_EVENT_TABLE(BasicDrawPane, wxPanel)

EVT_PAINT(BasicDrawPane::paintEvent)

END_EVENT_TABLE()

BasicDrawPane::BasicDrawPane(wxFrame* parent) : wxPanel(parent) {}

void BasicDrawPane::paintEvent(wxPaintEvent& evt)
{
    wxPaintDC dc(this);
    render(dc);
}


void BasicDrawPane::paintNow()
{
    wxClientDC dc(this);
    render(dc);
}

void BasicDrawPane::render(wxDC& dc)
{
    dc.SetFont(wxFont(14, wxFONTFAMILY_ROMAN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Font: wxFONTFAMILY_ROMAN, characterset: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789������������������,?.:-_�$��׽���( )=/%!+< >@{ }[ ]", 10, 20);

    dc.SetFont(wxFont(14, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Font: wxFONTFAMILY_TELETYPE, characterset: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789������������������,?.:-_�$��׽���( )=/%!+< >@{ }[ ]", 10, 60);

    dc.SetFont(wxFont(14, wxFONTFAMILY_DECORATIVE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Font: wxFONTFAMILY_DECORATIVE, characterset: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789������������������,?.:-_�$��׽���( )=/%!+< >@{ }[ ]", 10, 100);

    dc.SetFont(wxFont(14, wxFONTFAMILY_SCRIPT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Font: wxFONTFAMILY_SCRIPT, characterset: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789������������������,?.:-_�$��׽���( )=/%!+< >@{ }[ ]", 10, 140);

    dc.SetFont(wxFont(14, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Font: wxFONTFAMILY_SWISS, characterset: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789������������������,?.:-_�$��׽���( )=/%!+< >@{ }[ ]", 10, 180);

    dc.SetFont(wxFont(14, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Font: wxFONTFAMILY_MODERN, characterset: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789������������������,?.:-_�$��׽���( )=/%!+< >@{ }[ ]", 10, 220);


    dc.SetFont(wxFont(12, wxFONTFAMILY_ROMAN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Roman size 12", 20, 270);
    dc.SetFont(wxFont(14, wxFONTFAMILY_ROMAN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Roman size 14", 20, 300);
    dc.SetFont(wxFont(16, wxFONTFAMILY_ROMAN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Roman size 16", 20, 330);
    dc.SetFont(wxFont(18, wxFONTFAMILY_ROMAN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Roman size 18", 20, 360);

    dc.SetFont(wxFont(12, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Teletype size 12", 250, 270);
    dc.SetFont(wxFont(14, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Teletype size 14", 250, 300);
    dc.SetFont(wxFont(16, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Teletype size 16", 250, 330);
    dc.SetFont(wxFont(18, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Teletype size 18", 250, 360);

    dc.SetFont(wxFont(12, wxFONTFAMILY_DECORATIVE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Decorative size 12", 520, 260);
    dc.SetFont(wxFont(14, wxFONTFAMILY_DECORATIVE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Decorative size 14", 520, 290);
    dc.SetFont(wxFont(16, wxFONTFAMILY_DECORATIVE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Decorative size 16", 520, 320);
    dc.SetFont(wxFont(18, wxFONTFAMILY_DECORATIVE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Decorative size 18", 520, 350);

    dc.SetFont(wxFont(12, wxFONTFAMILY_SCRIPT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Script size 12", 710, 270);
    dc.SetFont(wxFont(14, wxFONTFAMILY_SCRIPT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Script size 14", 710, 300);
    dc.SetFont(wxFont(16, wxFONTFAMILY_SCRIPT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Script size 16", 710, 330);
    dc.SetFont(wxFont(18, wxFONTFAMILY_SCRIPT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Script size 18", 710, 360);


    dc.SetFont(wxFont(12, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Swiss size 12", 940, 270);
    dc.SetFont(wxFont(14, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Swiss size 14", 940, 300);
    dc.SetFont(wxFont(16, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Swiss size 16", 940, 330);
    dc.SetFont(wxFont(18, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Swiss size 18", 940, 360);

    dc.SetFont(wxFont(12, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Modern size 12", 1170, 270);
    dc.SetFont(wxFont(14, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Modern size 14", 1170, 300);
    dc.SetFont(wxFont(16, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Modern size 16", 1170, 330);
    dc.SetFont(wxFont(18, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false));
    dc.DrawText("Modern size 18", 1170, 360);

}