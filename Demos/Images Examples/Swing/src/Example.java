import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.Toolkit;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import java.awt.Color;

import javax.imageio.ImageIO;
import javax.swing.JFrame;



@SuppressWarnings("serial")
public class Example extends JFrame{

	Example() {
		setTitle("Example");
		setSize(600,330);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
	}
	
	public void paint(Graphics g) {
		Toolkit t = Toolkit.getDefaultToolkit();
		Image i = t.getImage("E:/_EGYETEM/6.felev/gyak2/Demos/Images Examples/UsedImages/java.png");
		g.drawImage(i,  30,  70,  this);
				
	}

	
	
	public static void main(String[] args)
	{
		Example e =new Example();
	}
}
