#include "wx/wx.h"
#include "wx/sizer.h"

class BasicDrawPane : public wxPanel
{

public:
    BasicDrawPane(wxFrame* parent);

    void paintEvent(wxPaintEvent& evt);
    void paintNow();

    void render(wxDC& dc);

    DECLARE_EVENT_TABLE()
};

class MyApp : public wxApp
{
    bool OnInit();

    wxFrame* frame;
    BasicDrawPane* drawPane;
public:

};

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
    wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
    frame = new wxFrame((wxFrame*)NULL, -1, wxT("Hello wxDC"), wxPoint(50, 50), wxSize(750, 430));

    drawPane = new BasicDrawPane((wxFrame*)frame);
    sizer->Add(drawPane, 1, wxEXPAND);

    frame->SetSizer(sizer);
    frame->SetAutoLayout(true);
    drawPane->SetBackgroundColour(*wxWHITE);
    frame->Show();
    return true;
}

BEGIN_EVENT_TABLE(BasicDrawPane, wxPanel)

EVT_PAINT(BasicDrawPane::paintEvent)

END_EVENT_TABLE()

BasicDrawPane::BasicDrawPane(wxFrame* parent) : wxPanel(parent) {}

void BasicDrawPane::paintEvent(wxPaintEvent& evt)
{
    wxPaintDC dc(this);
    render(dc);
}


void BasicDrawPane::paintNow()
{
    wxClientDC dc(this);
    render(dc);
}

void BasicDrawPane::render(wxDC& dc)
{
    /*
    wxImage img;
    img.LoadFile("E:/_EGYETEM/6.felev/gyak2/Demos/Images Examples/UsedImages/wxwidgets.png");
    wxBitmap bm;
    bm.LoadFile("E:/_EGYETEM/6.felev/gyak2/Demos/Images Examples/UsedImages/wxwidgets.png", wxBITMAP_DEFAULT_TYPE);
    */
    wxIcon icon("E://_EGYETEM/6.felev/gyak2/Demos/Images Examples/UsedImages/wxwidgets.jpg", wxICON_DEFAULT_TYPE, -1, -1);
    //dc.DrawBitmap(bm, 20, 20);
    dc.DrawIcon(icon, 30, 30);



}