#include "wx/wx.h"
#include "wx/sizer.h"

class BasicDrawPane : public wxPanel
{

public:
    BasicDrawPane(wxFrame* parent);

    void paintEvent(wxPaintEvent& evt);
    void paintNow();

    void render(wxDC& dc);

    DECLARE_EVENT_TABLE()
};

class MyApp : public wxApp
{
    bool OnInit();

    wxFrame* frame;
    BasicDrawPane* drawPane;
public:

};

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
    wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
    frame = new wxFrame((wxFrame*)NULL, -1, wxT("Hello wxDC"), wxPoint(50, 50), wxSize(1000, 500));

    drawPane = new BasicDrawPane((wxFrame*)frame);
    sizer->Add(drawPane, 1, wxEXPAND);

    frame->SetSizer(sizer);
    frame->SetAutoLayout(true);
    drawPane->SetBackgroundColour(*wxWHITE);
    frame->Show();
    return true;
}

BEGIN_EVENT_TABLE(BasicDrawPane, wxPanel)

EVT_PAINT(BasicDrawPane::paintEvent)

END_EVENT_TABLE()

BasicDrawPane::BasicDrawPane(wxFrame* parent) : wxPanel(parent) {}

void BasicDrawPane::paintEvent(wxPaintEvent& evt)
{
    wxPaintDC dc(this);
    render(dc);
}


void BasicDrawPane::paintNow()
{
    wxClientDC dc(this);
    render(dc);
}

void BasicDrawPane::render(wxDC& dc)
{
    wxColour black = wxColour(0, 0, 0);
    wxColour red = wxColour(255, 0, 0);
    wxColour blue = wxColour(0, 255, 0);
    wxColour green = wxColour(0, 0, 255);
    wxPen pen(black, 2, wxPENSTYLE_DOT);
    dc.SetPen(pen);
    dc.DrawLine(70, 20, 30, 100);

    pen.SetWidth(4);
    dc.SetPen(pen);
    dc.DrawLine(120, 20, 80, 100);

    pen.SetWidth(6);
    dc.SetPen(pen);
    dc.DrawLine(170, 20, 130, 100);
    

    pen.SetStyle(wxPENSTYLE_SHORT_DASH);
    pen.SetWidth(2);
    dc.SetPen(pen);
    dc.DrawLine(70, 120, 30, 200);

    pen.SetWidth(4);
    dc.SetPen(pen);
    dc.DrawLine(120, 120, 80, 200);

    pen.SetWidth(6);
    dc.SetPen(pen);
    dc.DrawLine(170, 120, 130, 200);


    pen.SetStyle(wxPENSTYLE_DOT_DASH);
    pen.SetWidth(2);
    dc.SetPen(pen);
    dc.DrawLine(70, 220, 30, 300);

    pen.SetWidth(4);
    dc.SetPen(pen);
    dc.DrawLine(120, 220, 80, 300);

    pen.SetWidth(6);
    dc.SetPen(pen);
    dc.DrawLine(170, 220, 130, 300);


    pen.SetStyle(wxPENSTYLE_LONG_DASH);
    pen.SetWidth(2);
    dc.SetPen(pen);
    dc.DrawLine(70, 320, 30, 400);

    pen.SetWidth(4);
    dc.SetPen(pen);
    dc.DrawLine(120, 320, 80, 400);

    pen.SetWidth(6);
    dc.SetPen(pen);
    dc.DrawLine(170, 320, 130, 400);

    pen.SetStyle(wxPENSTYLE_SOLID);
    dc.SetPen(pen);

    wxBrush brush(black, wxBRUSHSTYLE_SOLID);
    dc.SetBrush(brush);
    dc.DrawEllipse(250, 20, 300, 200);
    brush.SetColour(red);
    dc.SetBrush(brush);
    dc.DrawEllipse(600, 20, 300, 200);
    brush.SetColour(blue);
    dc.SetBrush(brush);
    dc.DrawEllipse(600, 250, 300, 200);
    brush.SetColour(green);
    dc.SetBrush(brush);
    dc.DrawEllipse(250, 250, 300, 200);
  
}