#include <stdio.h>
#include <gtk/gtk.h>
#include <math.h>

void draw_gradient1(cairo_t *);
void draw_gradient2(cairo_t *);
void draw_gradient3(cairo_t *);
void draw_gradient5(cairo_t *);
void draw_gradient6(cairo_t *);
void draw_gradient7(cairo_t *);



void activate (GtkApplication* app, gpointer user_data);

void on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data);

int main () {

  GtkApplication *app = gtk_application_new ("Example.app", G_APPLICATION_FLAGS_NONE);
  g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
  g_application_run (G_APPLICATION (app), 0, NULL);
  g_object_unref(app);

  return 0;
}

void on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data) {
    static const double dashed1[] = {4.0, 21.0, 2.0};

    cairo_save(cr);
    cairo_translate(cr, 10, 10);
    cairo_set_line_width(cr, 1.0);
    cairo_set_dash(cr, dashed1, 3, 0);
    cairo_move_to(cr, 40, 0);
    cairo_line_to(cr, 0, 80);
    cairo_stroke(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_translate(cr, 50, 10);
    cairo_set_line_width(cr, 2.0);
    cairo_set_dash(cr, dashed1, 3, 0);
    cairo_move_to(cr, 40, 0);
    cairo_line_to(cr, 0, 80);
    cairo_stroke(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_translate(cr, 90, 10);
    cairo_set_line_width(cr, 3.0);
    cairo_set_dash(cr, dashed1, 3, 0);
    cairo_move_to(cr, 40, 0);
    cairo_line_to(cr, 0, 80);
    cairo_stroke(cr);
    cairo_restore(cr);




    cairo_save(cr);
    cairo_translate(cr, 10, 110);
    cairo_set_line_width(cr, 1.0);
    cairo_set_dash(cr, dashed1, 2, 0);
    cairo_move_to(cr, 40, 0);
    cairo_line_to(cr, 0, 80);
    cairo_stroke(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_translate(cr, 50, 110);
    cairo_set_line_width(cr, 2.0);
    cairo_set_dash(cr, dashed1, 2, 0);
    cairo_move_to(cr, 40, 0);
    cairo_line_to(cr, 0, 80);
    cairo_stroke(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_translate(cr, 90, 110);
    cairo_set_line_width(cr, 3.0);
    cairo_set_dash(cr, dashed1, 2, 0);
    cairo_move_to(cr, 40, 0);
    cairo_line_to(cr, 0, 80);
    cairo_stroke(cr);
    cairo_restore(cr);





    cairo_save(cr);
    cairo_translate(cr, 10, 210);
    cairo_set_line_width(cr, 1.0);
    cairo_set_dash(cr, dashed1, 1, 0);
    cairo_move_to(cr, 40, 0);
    cairo_line_to(cr, 0, 80);
    cairo_stroke(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_translate(cr, 50, 210);
    cairo_set_line_width(cr, 2.0);
    cairo_set_dash(cr, dashed1, 1, 0);
    cairo_move_to(cr, 40, 0);
    cairo_line_to(cr, 0, 80);
    cairo_stroke(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_translate(cr, 90, 210);
    cairo_set_line_width(cr, 3.0);
    cairo_set_dash(cr, dashed1, 1, 0);
    cairo_move_to(cr, 40, 0);
    cairo_line_to(cr, 0, 80);
    cairo_stroke(cr);
    cairo_restore(cr);



    cairo_save(cr);
    cairo_translate(cr, 10, 310);
    cairo_set_line_width(cr, 1.0);
    cairo_set_dash(cr, dashed1, 0, 0);
    cairo_move_to(cr, 40, 0);
    cairo_line_to(cr, 0, 80);
    cairo_stroke(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_translate(cr, 50, 310);
    cairo_set_line_width(cr, 2.0);
    cairo_set_dash(cr, dashed1, 0, 0);
    cairo_move_to(cr, 40, 0);
    cairo_line_to(cr, 0, 80);
    cairo_stroke(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_translate(cr, 90, 310);
    cairo_set_line_width(cr, 3.0);
    cairo_set_dash(cr, dashed1, 0, 0);
    cairo_move_to(cr, 40, 0);
    cairo_line_to(cr, 0, 80);
    cairo_stroke(cr);
    cairo_restore(cr);


    cairo_save(cr);
    draw_gradient1(cr);
    cairo_restore(cr);

    cairo_save(cr);
    draw_gradient2(cr);
    cairo_restore(cr);

    cairo_save(cr);
    draw_gradient3(cr);
    cairo_restore(cr);

    cairo_save(cr);
    draw_gradient5(cr);
    cairo_restore(cr);

    cairo_save(cr);
    draw_gradient6(cr);
    cairo_restore(cr);

    cairo_save(cr);
    draw_gradient7(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_translate(cr, 1350, 100);
    cairo_set_antialias(cr, CAIRO_ANTIALIAS_SUBPIXEL);
    cairo_set_line_width(cr, 15.0);
    cairo_scale(cr, 1, 0.6);
    cairo_arc(cr, 0, 0, 150, 0, 2*M_PI);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
    cairo_restore(cr);

    cairo_save(cr);
    cairo_translate(cr, 1350, 300);
    cairo_set_line_width(cr, 15.0);
    cairo_scale(cr, 1, 0.6);
    cairo_arc(cr, 0, 0, 150, 0, 2*M_PI);
    cairo_set_source_rgb(cr, 0.3, 0.2, 0.1);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
    cairo_restore(cr);
}

void draw_gradient1(cairo_t *cr)
{
  cairo_pattern_t *pat1;
  pat1 = cairo_pattern_create_linear(-250.0, 100.0,  150.0, 250.0);

  gdouble j;
  gint count = 1;
  for ( j = 0.1; j < 1; j += 0.1 ) {
      if (( count % 2 ))  {
          cairo_pattern_add_color_stop_rgb(pat1, j, 0, 0, 0);
      } else {
          cairo_pattern_add_color_stop_rgb(pat1, j, 1, 0, 0);
      }
   count++;
  }



    cairo_save(cr);
    cairo_translate(cr, 300, 100);
    cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT);
    cairo_set_line_width(cr, 15.0);
    cairo_scale(cr, 1, 0.6);
    cairo_arc(cr, 0, 0, 150, 0, 2*M_PI);
    cairo_set_source(cr, pat1);
    cairo_fill(cr);
    cairo_restore(cr);

    cairo_pattern_destroy(pat1);
}

void draw_gradient2(cairo_t *cr)
{
  cairo_pattern_t *pat2;
  pat2 = cairo_pattern_create_linear(-250.0, 0.0,  200.0, 0.0);

  gdouble i;
  gint count = 1;
  for ( i = 0.05; i < 0.95; i += 0.025 ) {
      if (( count % 2 ))  {
          cairo_pattern_add_color_stop_rgb(pat2, i, 0, 0, 0);
      } else {
          cairo_pattern_add_color_stop_rgb(pat2, i, 0, 0, 1);
      }
   count++;
  }

  cairo_save(cr);
  cairo_translate(cr, 650, 100);
  cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
  cairo_set_line_width(cr, 15.0);
  cairo_scale(cr, 1, 0.6);
  cairo_arc(cr, 0, 0, 150, 0, 2*M_PI);
  cairo_set_source(cr, pat2);
  cairo_fill(cr);
  cairo_restore(cr);

  cairo_pattern_destroy(pat2);
}

void draw_gradient3(cairo_t *cr)
{
  cairo_pattern_t *pat3;
  pat3 = cairo_pattern_create_linear(-250.0, 260.0, 200.0, 360.0);

  cairo_pattern_add_color_stop_rgb(pat3, 0.1, 0, 0, 0);
  cairo_pattern_add_color_stop_rgb(pat3, 0.5, 1, 1, 0);
  cairo_pattern_add_color_stop_rgb(pat3, 0.9, 0, 0, 0);

  cairo_save(cr);
  cairo_translate(cr, 1000, 100);
  cairo_set_antialias(cr, CAIRO_ANTIALIAS_GRAY);
  cairo_set_line_width(cr, 15.0);
  cairo_scale(cr, 1, 0.6);
  cairo_arc(cr, 0, 0, 150, 0, 2*M_PI);
  cairo_set_source(cr, pat3);
  cairo_fill(cr);
  cairo_restore(cr);

  cairo_pattern_destroy(pat3);
}


void draw_gradient5(cairo_t *cr)
{
  cairo_pattern_t *pat1;
  pat1 = cairo_pattern_create_linear(-250.0, 260.0,  150.0, 100.0);

  gdouble j;
  gint count = 1;
  for ( j = 0.1; j < 1; j += 0.1 ) {
      if (( count % 2 ))  {
          cairo_pattern_add_color_stop_rgb(pat1, j, 0, 0, 0);
      } else {
          cairo_pattern_add_color_stop_rgb(pat1, j, 1, 0, 0);
      }
   count++;
  }



    cairo_save(cr);
    cairo_translate(cr, 300, 300);
    cairo_set_antialias(cr, CAIRO_ANTIALIAS_FAST);
    cairo_set_line_width(cr, 15.0);
    cairo_scale(cr, 1, 0.6);
    cairo_arc(cr, 0, 0, 150, 0, 2*M_PI);
    cairo_set_source(cr, pat1);
    cairo_fill(cr);
    cairo_restore(cr);

    cairo_pattern_destroy(pat1);
}

void draw_gradient6(cairo_t *cr)
{
  cairo_pattern_t *pat2;
  pat2 = cairo_pattern_create_linear(-250.0, 100.0,  200.0, 200.0);

  gdouble i;
  gint count = 1;
  for ( i = 0.05; i < 0.95; i += 0.025 ) {
      if (( count % 2 ))  {
          cairo_pattern_add_color_stop_rgb(pat2, i, 0, 0, 0);
      } else {
          cairo_pattern_add_color_stop_rgb(pat2, i, 0, 0, 1);
      }
   count++;
  }

  cairo_save(cr);
  cairo_translate(cr, 650, 300);
  cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
  cairo_set_line_width(cr, 15.0);
  cairo_scale(cr, 1, 0.6);
  cairo_arc(cr, 0, 0, 150, 0, 2*M_PI);
  cairo_set_source(cr, pat2);
  cairo_fill(cr);
  cairo_restore(cr);

  cairo_pattern_destroy(pat2);
}

void draw_gradient7(cairo_t *cr)
{
  cairo_pattern_t *pat3;
  pat3 = cairo_pattern_create_linear(-250.0, 20.0, 200.0, 360.0);

  cairo_pattern_add_color_stop_rgb(pat3, 0.1, 0, 0, 0);
  cairo_pattern_add_color_stop_rgb(pat3, 0.5, 1, 1, 0);
  cairo_pattern_add_color_stop_rgb(pat3, 0.9, 0, 0, 0);

  cairo_save(cr);
  cairo_translate(cr, 1000, 300);
  cairo_set_antialias(cr, CAIRO_ANTIALIAS_BEST);
  cairo_set_line_width(cr, 15.0);
  cairo_scale(cr, 1, 0.6);
  cairo_arc(cr, 0, 0, 150, 0, 2*M_PI);
  cairo_set_source(cr, pat3);
  cairo_fill(cr);
  cairo_restore(cr);

  cairo_pattern_destroy(pat3);
}



void activate (GtkApplication* app, gpointer user_data) {

  GtkWidget *window = gtk_application_window_new(app);
  gtk_window_set_title (GTK_WINDOW (window), "Example");
  GtkWidget *drawing_area = gtk_drawing_area_new ();
  gtk_widget_set_size_request (drawing_area, 1600, 420);
  gtk_container_add(GTK_CONTAINER(window), drawing_area);
  g_signal_connect(G_OBJECT(drawing_area), "draw", G_CALLBACK(on_draw_event), NULL);
  gtk_widget_show_all(window);
}
