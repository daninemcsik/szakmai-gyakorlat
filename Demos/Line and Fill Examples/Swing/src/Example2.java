import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.geom.*;
import java.awt.Color;

import javax.swing.JFrame;



@SuppressWarnings("serial")
public class Example2 extends JFrame{

	public Example2() {
		setTitle("Example");
		setSize(1600,280);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
	}
	
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		
		Stroke dashed = new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0);
		g2.setStroke(dashed);
		g2.drawLine(70, 40, 30, 120);
		dashed = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL, 0, new float[]{11}, 0);
		g2.setStroke(dashed);
		g2.drawLine(120, 40, 80, 120);
		dashed = new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL, 0, new float[]{5}, 0);
		
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setStroke(dashed);
		g2.drawLine(170, 40, 130, 120);
		
		dashed = new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL, 0, new float[]{1}, 0);
		g2.setStroke(dashed);
		
		g2.setPaint(Color.black);
		g2.fill(new Ellipse2D.Double(200, 50, 300, 200));
		
		GradientPaint redtowhite = new GradientPaint(300,50,Color.RED, 525, 500,Color.WHITE);
		g2.setPaint(redtowhite);
		g2.fill(new Ellipse2D.Double(525, 50, 300, 200));
		
		GradientPaint bluetoblack = new GradientPaint(525,50,Color.BLUE, 850, 500,Color.BLACK);
		g2.setPaint(bluetoblack);
		g2.fill(new Ellipse2D.Double(850, 50, 300, 200));
		
		g2.draw(new Ellipse2D.Double(1175, 50, 300, 200));
		
		
		
		
	}

	
	
	public static void main(String[] args) {
		Example2 e = new Example2();
	}
}
