#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter obj(this);

    QPen pen(Qt::black, 3, Qt::DotLine, Qt::RoundCap, Qt::RoundJoin);
    obj.setPen(pen);

    obj.drawLine(70, 20, 30, 100);
    obj.drawLine(120, 20, 80, 100);
    obj.drawLine(170, 20, 130, 100);

    pen.setStyle(Qt::DashLine);
    obj.setPen(pen);
    obj.drawLine(70, 120, 30, 200);
    obj.drawLine(120, 120, 80, 200);
    obj.drawLine(170, 120, 130, 200);

    pen.setStyle(Qt::DashDotLine);
    obj.setPen(pen);
    obj.drawLine(70, 220, 30, 300);
    obj.drawLine(120, 220, 80, 300);
    obj.drawLine(170, 220, 130, 300);


    pen.setStyle(Qt::DashDotDotLine);
    obj.setPen(pen);
    obj.drawLine(70, 320, 30, 400);
    obj.drawLine(120, 320, 80, 400);
    obj.drawLine(170, 320, 130, 400);

    pen.setStyle(Qt::SolidLine);
    obj.setPen(pen);
    obj.drawLine(70, 420, 30, 500);
    obj.drawLine(120, 420, 80, 500);
    obj.drawLine(170, 420, 130, 500);

    QBrush brush(Qt::black, Qt::SolidPattern);
    obj.setBrush(brush);
    obj.drawEllipse(250, 20, 300, 200);

    QRadialGradient gradient(350, 300, 150, 450, 250);
    gradient.setColorAt(0, QColor::fromRgbF(0, 1, 0, 1));
    gradient.setColorAt(1, QColor::fromRgbF(0, 0, 1, 1));
    QBrush brush2(gradient);
    obj.setBrush(brush2);
    obj.drawEllipse(250, 250, 300, 200);

    brush.setStyle(Qt::Dense2Pattern);
    obj.setBrush(brush);
    obj.drawEllipse(600, 20, 300, 200);

    QRadialGradient gradient2(700, 300, 150, 800, 250);
    gradient2.setColorAt(0, QColor::fromRgbF(1, 0, 1, 1));
    gradient2.setColorAt(1, QColor::fromRgbF(0, 1, 0, 1));
    QBrush brush3(gradient2);
    obj.setBrush(brush3);
    obj.drawEllipse(600, 250, 300, 200);

    obj.setRenderHint(QPainter::Antialiasing);
    brush.setStyle(Qt::Dense6Pattern);
    obj.setBrush(brush);
    obj.drawEllipse(950, 20, 300, 200);

    brush.setStyle(Qt::Dense7Pattern);
    obj.setBrush(brush);
    obj.drawEllipse(950, 250, 300, 200);

    brush.setStyle(Qt::HorPattern);
    obj.setBrush(brush);
    obj.drawEllipse(1300, 20, 300, 200);

    brush.setStyle(Qt::VerPattern);
    obj.setBrush(brush);
    obj.drawEllipse(1300, 250, 300, 200);

}
