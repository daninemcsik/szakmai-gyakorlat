Computer graphics can be created as either raster or vector images. Raster graphics are bitmaps. 
A bitmap is a grid of individual pixels that collectively compose an image. Raster graphics render images as a collection of countless tiny squares. Each square, or pixel, is coded in a specific hue or shade.
Vector graphics are based on mathematical formulas that define geometric primitives such as polygons, lines, curves, circles and rectangles. Because vector graphics are composed of true geometric primitives, they are best used to represent more structured images, like line art graphics with flat, uniform colors.
* https://www.printcnx.com/resources-and-support/addiational-resources/raster-images-vs-vector-graphics/


# Java swing:

When creating a Graphics2D object, the GraphicsConfiguration specifies the default transform for the 
target of the Graphics2D (a Component or Image). This default transform maps the user space coordinate 
system to screen and printer device coordinates such that the origin maps to the upper left hand corner 
of the target region of the device with increasing X coordinates extending to the right and increasing Y 
coordinates extending downward. 

The steps in the rendering process are:
Determine what to render.
Constrain the rendering operation to the current Clip. The Clip is specified by a Shape in user space and is controlled by the program using the various clip manipulation methods of Graphics and Graphics2D. This user clip is transformed into device space by the current Transform and combined with the device clip, which is defined by the visibility of windows and device extents. The combination of the user clip and device clip defines the composite clip, which determines the final clipping region. The user clip is not modified by the rendering system to reflect the resulting composite clip.
Determine what colors to render.
Apply the colors to the destination drawing surface using the current Composite attribute in the Graphics2D context.
* https://docs.oracle.com/javase/8/docs/api/java/awt/Graphics2D.html



# QT: 

Qt's paint system enables painting on screen and print devices using the same API, and is primarily based on the QPainter, QPaintDevice, and QPaintEngine classes.
QPainter is used to perform drawing operations, QPaintDevice is an abstraction of a two-dimensional space that can be painted on using a QPainter, and QPaintEngine provides the interface that the painter uses to draw onto different types of devices. The QPaintEngine class is used internally by QPainter and QPaintDevice, and is hidden from application programmers unless they create their own device type.
* https://doc.qt.io/qt-6/paintsystem.html

QPainter provides highly optimized functions to do most of the drawing GUI programs require. It can draw everything from simple lines to complex shapes like pies and chords. It can also draw aligned text and pixmaps. Normally, it draws in a "natural" coordinate system, but it can also do view and world transformation. QPainter can operate on any object that inherits the QPaintDevice class.
The common use of QPainter is inside a widget's paint event: Construct and customize (e.g. set the pen or the brush) the painter. Then draw.
* https://doc.qt.io/qt-6/qpainter.html#details

Qt provides several premade implementations of QPaintEngine for the different painter backends we support. The primary paint engine provided is the raster paint engine, which contains a software rasterizer which supports the full feature set on all supported platforms. This is the default for painting on QWidget-based classes in e.g. on Windows, X11 and macOS, it is the backend for painting on QImage and it is used as a fallback for paint engines that do not support a certain capability.
If one wants to use QPainter to draw to a different backend, one must subclass QPaintEngine and reimplement all its virtual functions. The QPaintEngine implementation is then made available by subclassing QPaintDevice and reimplementing the virtual function QPaintDevice::paintEngine().
QPaintEngine is created and owned by the QPaintDevice that created it.
* https://doc.qt.io/qt-6/qpaintengine.html

A paint device is an abstraction of a two-dimensional space that can be drawn on using a QPainter. Its default coordinate system has its origin located at the top-left position. X increases to the right and Y increases downwards. The unit is one pixel.
The drawing capabilities of QPaintDevice are currently implemented by the QWidget, QImage, QPixmap, QPicture, and QPrinter subclasses.
To implement support for a new backend, you must derive from QPaintDevice and reimplement the virtual paintEngine() function to tell QPainter which paint engine should be used to draw on this particular device. Note that you also must create a corresponding paint engine to be able to draw on the device, i.e derive from QPaintEngine and reimplement its virtual functions.
* https://doc.qt.io/qt-6/qpaintdevice.html

Scalable Vector Graphics (SVG) is an XML-based language for describing two-dimensional vector graphics. 
Qt provides classes for rendering and displaying SVG drawings in widgets and on other paint devices.
* https://doc.qt.io/qt-6/qtsvg-index.html


# WPF:

WPF uses vector graphics as its rendering data format. Vector graphics—which include Scalable Vector Graphics (SVG), Windows metafiles (.wmf), and TrueType fonts—store rendering data and transmit it as a list of instructions that describe how to recreate an image using graphics primitives. For example, TrueType fonts are outline fonts that describe a set of lines, curves, and commands, rather than an array of pixels. One of the key benefits of vector graphics is the ability to scale to any size and resolution.
* https://docs.microsoft.com/en-us/dotnet/desktop/wpf/graphics-multimedia/wpf-graphics-rendering-overview?view=netframeworkdesktop-4.8

The Visual object is a core WPF object, whose primary role is to provide rendering support
* https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.visual?view=net-5.0

DrawingVisual is a visual object that can be used to render vector graphics on the screen. The content is persisted by the system.
DrawingVisual is a lightweight drawing class that is used to render shapes, images, or text. This class is considered lightweight because it does not provide layout, input, focus, or event handling, which improves its performance.
* https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.drawingvisual?view=net-5.0

A Drawing object describes visible content, such as a shape, bitmap, video, or a line of text. Different types of drawings describe different types of content. The following is a list of the different types of drawing objects. (GeometryDrawing – Draws a shape, ImageDrawing – Draws an image, GlyphRunDrawing – Draws text, VideoDrawing – Plays an audio or video file. DrawingGroup – Draws other drawings. Use a drawing group to combine other drawings into a single composite drawing)
* https://docs.microsoft.com/en-us/dotnet/desktop/wpf/graphics-multimedia/drawing-objects-overview?view=netframeworkdesktop-4.8


# wxWidgets:

Device context, generalizing the concept of a drawing surface such as a window or a printed page.
A device context has a coordinate system with its origin at the top-left of the surface. You can use SetAxisOrientation if you prefer, say, the y-axis to go from bottom to top.
Use wxClientDC objects to draw on the client area of windows outside of paint events.
An alternative to using wxClientDC directly is to use wxBufferedDC, which stores your drawing in a memory device context and transfers it to the window in one step when the device context is about to be deleted. This can result in smoother updates—for example, if you don’t want the user to see a complex graphic being updated bit by bit.
If you define a paint event handler, you must always create a wxPaintDC object, even if you don’t use it. Creating this object will tell wxWidgets that the invalid regions in the window have been repainted so that the windowing system won’t keep sending paint events infinitely.
A window receives two kinds of paint event: wxPaintEvent for drawing the main graphic, and wxEraseEvent for painting the background. 
wxBufferedPaintDC is a buffered version of wxPaintDC. Simply replace wxPaintDC with wxBufferedPaintDC in your paint event handler, and the graphics will be drawn to a bitmap before being drawn all at once on the window, reducing flicker
A memory device context has a bitmap associated with it, so that drawing into the device context draws on the bitmap. First create a wxMemoryDC object with the default constructor, and then use SelectObject to associate a bitmap with the device context.
Use wxScreenDC for drawing on areas of the whole screen.
* https://ptgmedia.pearsoncmg.com/images/0131473816/downloads/0131473816_book.pdf (page 131-140)

wxWidgets supports four kinds of bitmap images: wxBitmap, wxIcon, wxCursor, and wxImage.
wxBitmap represents a platform-dependent bitmap, with an optional wxMask to support drawing with transparency.
wxIcon represents the platform’s concept of an icon, a small image with transparency that can be used for giving frames and dialogs a recognizable visual cue, among other things. 
wxCursor represents the mouse pointer image.
wxImage is the only class of the four with a platform-independent implementation, supporting 24-bit images with an optional alpha channel. 
Unlike a wxBitmap, a wxImage cannot be drawn directly to a wxDC. Instead, a wxBitmap object must be created from the wxImage. This bitmap can then be drawn in a device context by using wxDC::DrawBitmap.
Things you can do with a wxBitmap:
Draw it on a window via a device context.
Use it as a bitmap label for classes such as wxBitmapButton, wxStaticBitmap, and wxToolBar.
Use it to implement double buffering (drawing into an off-screen wxMemoryDC before drawing to a window).
(page 265-268)


# Gtk:

Since GTK 3.0 all rendering is done through Cairo.

The Cairo drawing model relies on a three layer model.

Any drawing process takes place in three steps:

First a mask is created, which includes one or more vector primitives or forms, i.e., circles, squares, TrueType fonts, Bézier curves, etc.
Then source must be defined, which may be a color, a color gradient, a bitmap or some vector graphics, and from the painted parts of this source a die cut is made with the help of the above defined mask.
Finally the result is transferred to the destination or surface, which is provided by the back-end for the output.
* https://en.wikipedia.org/wiki/Cairo_(graphics)



# Canvas:

The HTML canvas element is used to draw “raster” graphics on a web application. The Canvas API provides two drawing contexts: 2D and 3D
Canvas is a raster graphics API — you manipulate stuff at the pixel level. That means the underlying software does not know the model you are using to display your context — it doesn’t know if you’re drawing a rectangle or a circle.
Paths are a collection of points in the 2D pixel grid in the canvas. They are drawn with the help of this API.
The ImageData object can be used to manipulate individual pixels.
* https://www.freecodecamp.org/news/full-overview-of-the-html-canvas-6354216fba8d/