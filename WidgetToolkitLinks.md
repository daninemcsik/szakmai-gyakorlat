# Java:

## 2D graphics:

* https://docs.oracle.com/javase/tutorial/2d/index.html
* https://docs.oracle.com/javase/tutorial/2d/overview/index.html
* https://en.wikipedia.org/wiki/Java_2D

## Abstract Window Toolkit (AWT):

* https://docs.oracle.com/javase/7/docs/api/java/awt/package-summary.html
* https://en.wikipedia.org/wiki/Abstract_Window_Toolkit

## Standard Window Toolkit (SWT):

* https://www.eclipse.org/swt/javadoc.php
* https://en.wikipedia.org/wiki/Standard_Widget_Toolkit

## Java Swing:

* https://docs.oracle.com/javase/7/docs/api/javax/swing/package-summary.html
* https://en.wikipedia.org/wiki/Swing_(Java)
	
## JavaFX:

* https://docs.oracle.com/javase/8/javafx/api/toc.htm
* https://en.wikipedia.org/wiki/JavaFX


# C++:

## QT:

* https://doc.qt.io/qt-6/qtwidgets-index.html
* https://doc.qt.io/qt-5/qpainter.html
* https://doc.qt.io/qt-6/qtwidgets-module.html
* https://doc.qt.io/qt-6/qtopengl-module.html
* https://doc.qt.io/qt-6/qtdatavisualization-module.html
* https://doc.qt.io/qt-6/topics-graphics.html
* https://en.wikipedia.org/wiki/Qt_(software)

## wxWidgets:

* https://wiki.wxwidgets.org/Documentation
* https://en.wikipedia.org/wiki/WxWidgets

## GTK:

* https://www.gtk.org/docs/language-bindings/cpp
* https://docs.gtk.org/gtk4/#classes
* https://en.wikipedia.org/wiki/Gtkmm

## Windows API:

* https://docs.microsoft.com/en-us/cpp/mfc/mfc-desktop-applications?view=msvc-160
* https://docs.microsoft.com/hu-hu/windows/win32/gdiplus/-gdiplus-gdi-start?redirectedfrom=MSDN

## Fast Light Toolkit (FLTK):

* https://www.fltk.org
* https://www.fltk.org/doc-1.3/annotated.html
* https://en.wikipedia.org/wiki/FLTK

## Simple DirectMedia Layer (SDL):

* https://www.libsdl.org/download-2.0.php
* https://en.wikipedia.org/wiki/Simple_DirectMedia_Layer

# XML / C#:

## Windows Presentation Foundation (WPF):

* https://docs.microsoft.com/en-us/dotnet/desktop/wpf/?view=netdesktop-5.0
* https://en.wikipedia.org/wiki/Windows_Presentation_Foundation

# HTML:

## HTML Canvas:

* https://www.w3schools.com/graphics/canvas_intro.asp
* https://www.w3schools.com/html/html5_canvas.asp

## HTML SVG:

* https://www.w3schools.com/graphics/svg_intro.asp
* https://www.w3schools.com/graphics/svg_inhtml.asp
* https://www.w3schools.com/html/html5_svg.asp















