## Fogalmak:  
#### rendering, drawing model, component, container

Component-nek nevezünk egy olyan objektumot, ami grafikusan megjelenik a képernyőnkön és
a felhasználó valamilyen szinten tud vele kommunikálni.
Ilyen component-ek közé tartozik például a Button, Container, Label, ...
Componentek külön-külön megjeleníthetők, viszont a dekstop application-ök nagy része
egy Container inicializálásával kezdődik.
Container-ek olyan egységek amikre rá lehet tenni több component-et 
(Layout, scroll bar,...).

# Java Swing:

#### Hogyan rajzol a képernyőre?
* https://docs.oracle.com/javase/7/docs/api/java/awt/Graphics2D.html


When creating a Graphics2D object, the GraphicsConfiguration specifies the default transform for the 
target of the Graphics2D (a Component or Image). This default transform maps the user space coordinate 
system to screen and printer device coordinates such that the origin maps to the upper left hand corner 
of the target region of the device with increasing X coordinates extending to the right and increasing Y 
coordinates extending downward. 

The steps in the rendering process are:

Determine what to render.
Constrain the rendering operation to the current Clip. The Clip is specified by a Shape in user space and is controlled by the program using the various clip manipulation methods of Graphics and Graphics2D. This user clip is transformed into device space by the current Transform and combined with the device clip, which is defined by the visibility of windows and device extents. The combination of the user clip and device clip defines the composite clip, which determines the final clipping region. The user clip is not modified by the rendering system to reflect the resulting composite clip.
Determine what colors to render.
Apply the colors to the destination drawing surface using the current Composite attribute in the Graphics2D context.



# QT:

####  Hogyan rajzol a képernyőre?
* QPainter - draw vector graphics, text, images (QPaintDevice -> QPaintEngine) 
* https://doc.qt.io/qt-5/qpaintengine.html#details
* https://doc.qt.io/qt-5/qtquick-visualcanvas-scenegraph.html


The rendering of the scene graph happens internally in the QQuickWindow class, and there is no public API to access it. 
There are, however, a few places in the rendering pipeline where the user can attach application code. 
This can be used to add custom scene graph content or to insert arbitrary rendering commands by directly calling the graphics API (OpenGL, Vulkan, Metal, etc.) that is in use by the scene graph. The integration points are defined by the render loop.

There are three render loop variants available: basic, windows, and threaded. Out of these, basic and windows are single-threaded, while threaded performs scene graph rendering on a dedicated thread. Qt attempts to choose a suitable loop based on the platform and possibly the graphics drivers in use.


# WPF:

####  Hogyan rajzol a képernyőre?
*https://docs.microsoft.com/en-us/dotnet/desktop/wpf/graphics-multimedia/wpf-graphics-rendering-overview?view=netframeworkdesktop-4.8
*https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.drawingvisual?view=net-5.0


A Visual object stores its render data as a vector graphics instruction list. Each item in the instruction list represents a low-level set of graphics data and associated resources in a serialized format.

The DrawingContext allows you to populate a Visual with visual content. When you use a DrawingContext object's draw commands, you are actually storing a set of render data that will later be used by the graphics system; you are not drawing to the screen in real-time.

When you create a WPF control, such as a Button, the control implicitly generates render data for drawing itself. For example, setting the Content property of the Button causes the control to store a rendering representation of a glyph.

A Visual describes its content as one or more Drawing objects contained within a DrawingGroup. A DrawingGroup also describes opacity masks, transforms, bitmap effects, and other operations that are applied to its contents. DrawingGroup operations are applied in the following order when content is rendered: OpacityMask, Opacity, BitmapEffect, ClipGeometry, GuidelineSet, and then Transform.

A user interface element, such as a Button control, contains several vector graphics instruction lists that describe the 
entire rendering definition of a control.


# wxWidgets:

####  Hogyan rajzol a képernyőre?
* https://docs.wxwidgets.org/3.0/classwx_renderer_native.html
* https://docs.wxwidgets.org/3.0/group__group__class__gdi.html
* https://docs.wxwidgets.org/3.0/overview_dc.html


Usually wxWidgets uses the underlying low level GUI system to draw all the controls - this is what we mean when we say that it is a "native" framework. However not all controls exist under all (or even any) platforms and in this case wxWidgets provides a default, generic, implementation of them written in wxWidgets itself.

These controls don't have the native appearance if only the standard line drawing and other graphics primitives are used, because the native appearance is different under different platforms while the lines are always drawn in the same way.

This is why we have renderers: wxRendererNative is a class which virtualizes the drawing, i.e. it abstracts the drawing operations and allows you to draw say, a button, without caring about exactly how this is done. Of course, as we can draw the button differently in different renderers, this also allows us to emulate the native look and feel.

So the renderers work by exposing a large set of high-level drawing functions which are used by the generic controls. There is always a default global renderer but it may be changed or extended by the user.


# GTK:

####  Hogyan rajzol a képernyőre?
* https://docs.gtk.org/gtk3/drawing-model.html


In practice, most windows in modern GTK application are client-side constructs. Only few windows (in particular toplevel windows) are native, which means that they represent a window from the underlying windowing system on which GTK is running. For example, on X11 it corresponds to a Window; on Windows, it corresponds to a HANDLE.

Generally, the drawing cycle begins when GTK receives an exposure event from the underlying windowing system: if the user drags a window over another one, the windowing system will tell the underlying window that it needs to repaint itself. The drawing cycle can also be initiated when a widget itself decides that it needs to update its display. For example, when the user types a character in a GtkEntry widget, the entry asks GTK to queue a redraw operation for itself.

The windowing system generates events for native windows. The GDK interface to the windowing system translates such native events into GdkEvent structures and sends them on to the GTK layer. In turn, the GTK layer finds the widget that corresponds to a particular GdkWindow and emits the corresponding event signals on that widget.


# Canvas:

####  Hogyan rajzol a képernyőre?
* https://html.spec.whatwg.org/multipage/canvas.html#drawing-model
* https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Client-side_web_APIs/Drawing_graphics


When a shape or image is painted, user agents must follow these steps, in the order given (or act as if they do):

Render the shape or image onto an infinite transparent black bitmap, creating image A, as described in the previous sections. For shapes, the current fill, stroke, and line styles must be honored, and the stroke must itself also be subjected to the current transformation matrix.

When the filter attribute is set to a value other than 'none' and all the externally-defined filters it references, if any, are in documents that are currently loaded, then use image A as the input to the filter, creating image B. Otherwise, let B be an alias for A.

When shadows are drawn, render the shadow from image B, using the current shadow styles, creating image C.
When shadows are drawn, multiply the alpha component of every pixel in C by globalAlpha.
When shadows are drawn, composite C within the clipping region over the current output bitmap using the current composition operator.

Multiply the alpha component of every pixel in B by globalAlpha.

Composite B within the clipping region over the current output bitmap using the current composition operator.

When compositing onto the output bitmap, pixels that would fall outside of the output bitmap must be discarded.