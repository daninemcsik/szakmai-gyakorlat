# Bevezető:
* https://doc.qt.io/qt-5/qapplication.html#details
* https://doc.qt.io/qt-6/qwindow.html#details


The QApplication class manages the GUI application's control flow and main settings. 

QApplication specializes QGuiApplication with some functionality needed for QWidget-based applications. It handles widget specific 
initialization, finalization.

QApplication's main areas of responsibility are:
-It initializes the application with the user's desktop settings such as palette(), font() and doubleClickInterval(). It keeps track 
of these properties in case the user changes the desktop globally, for example through some kind of control panel.
-It performs event handling, meaning that it receives events from the underlying window system and dispatches them to the relevant widgets. 
By using sendEvent() and postEvent() you can send your own events to widgets.
-It defines the application's look and feel, which is encapsulated in a QStyle object. This can be changed at runtime with setStyle().


The QWindow class represents a window in the underlying windowing system.

An application will typically use QWidget or QQuickView for its UI, and not QWindow directly. Still, it is possible to render directly 
to a QWindow with QBackingStore or QOpenGLContext, when wanting to keep dependencies to a minimum or when wanting to use OpenGL directly.

Windows can potentially use a lot of memory. A usual measurement is width times height times color depth. A window might also include multiple buffers to support double and triple buffering, as well as depth and stencil buffers. To release a window's memory resources, call the destroy() function.

By default, the window is not visible, and you must call setVisible(true), or show() or similar to make it visible.
Depending on the underlying system, a visible window might still not be shown on the screen. It could, for instance, be covered by other opaque windows or moved outside the physical area of the screen. On windowing systems that have exposure notifications, the isExposed() accessor describes whether the window should be treated as directly visible on screen.

There are two Qt APIs that can be used to render content into a window, QBackingStore for rendering with a QPainter and flushing the contents to a window with type QSurface::RasterSurface, and QOpenGLContext for rendering with OpenGL to a window with type QSurface::OpenGLSurface.




# Drawing:
* https://doc.qt.io/qt-6/paintsystem.html
* https://doc.qt.io/qt-6/qpainter.html#details
* https://doc.qt.io/qt-6/qpaintengine.html
* https://doc.qt.io/qt-6/qpaintdevice.html


Qt's paint system enables painting on screen and print devices using the same API, and is primarily based on the QPainter, QPaintDevice, and QPaintEngine classes. QPainter is used to perform drawing operations, QPaintDevice is an abstraction of a two-dimensional space that can be painted on using a QPainter, and QPaintEngine provides the interface that the painter uses to draw onto different types of devices. The QPaintEngine class is used internally by QPainter and QPaintDevice, and is hidden from application programmers unless they create their own device type. 


QPainter provides highly optimized functions to do most of the drawing GUI programs require. It can draw everything from simple lines to complex shapes like pies and chords. It can also draw aligned text and pixmaps. Normally, it draws in a "natural" coordinate system, but it can also do view and world transformation. QPainter can operate on any object that inherits the QPaintDevice class. The common use of QPainter is inside a widget's paint event: Construct and customize (e.g. set the pen or the brush) the painter. Then draw. 


Qt provides several premade implementations of QPaintEngine for the different painter backends we support. The primary paint engine provided is the raster paint engine, which contains a software rasterizer which supports the full feature set on all supported platforms. This is the default for painting on QWidget-based classes in e.g. on Windows, X11 and macOS, it is the backend for painting on QImage and it is used as a fallback for paint engines that do not support a certain capability. If one wants to use QPainter to draw to a different backend, one must subclass QPaintEngine and reimplement all its virtual functions. The QPaintEngine implementation is then made available by subclassing QPaintDevice and reimplementing the virtual function QPaintDevice::paintEngine(). QPaintEngine is created and owned by the QPaintDevice that created it. 


A paint device is an abstraction of a two-dimensional space that can be drawn on using a QPainter. Its default coordinate system has its origin located at the top-left position. X increases to the right and Y increases downwards. The unit is one pixel. The drawing capabilities of QPaintDevice are currently implemented by the QWidget, QImage, QPixmap, QPicture, and QPrinter subclasses. To implement support for a new backend, you must derive from QPaintDevice and reimplement the virtual paintEngine() function to tell QPainter which paint engine should be used to draw on this particular device. Note that you also must create a corresponding paint engine to be able to draw on the device, i.e derive from QPaintEngine and reimplement its virtual functions. 



# Coloring:
* https://doc.qt.io/qt-6/qcolor.html
* https://doc.qt.io/qt-6/qpalette.html


The QColor class provides colors based on RGB, HSV or CMYK values.

A color is normally specified in terms of RGB (red, green, and blue) components, but it is also possible to specify it in terms of HSV (hue, saturation, and value) and CMYK (cyan, magenta, yellow and black) components. In addition a color can be specified using a color name.
QColor also support alpha-blended outlining and filling. The alpha channel of a color specifies the transparency effect, 0 represents a fully transparent color, while 255 represents a fully opaque color. 
(kép)

HSV, like RGB, has three components:
-H, for hue, is in the range 0 to 359 if the color is chromatic (not gray), or meaningless if it is gray. It represents degrees on the color wheel familiar to most people. Red is 0 (degrees), green is 120, and blue is 240.
-S, for saturation, is in the range 0 to 255, and the bigger it is, the stronger the color is. Grayish colors have saturation near 0; very strong colors have saturation near 255.
-V, for value, is in the range 0 to 255 and represents lightness or brightness of the color. 0 is black; 255 is as far from black as possible.

HSL is similar to HSV, however instead of the Value parameter, HSL specifies a Lightness parameter which maps somewhat differently to the brightness of the color.

CMYK has four components, all in the range 0-255: cyan (C), magenta (M), yellow (Y) and black (K). Cyan, magenta and yellow are called subtractive colors; the CMYK color model creates color by starting with a white surface and then subtracting color by applying the appropriate components.

The QColor constructor creates the color based on RGB values. To create a QColor based on either HSV or CMYK values, use the toHsv() and toCmyk() functions respectively. These functions return a copy of the color using the desired format. In addition the static fromRgb(), fromHsv() and fromCmyk() functions create colors from the specified values. Alternatively, a color can be converted to any of the three formats using the convertTo() function (returning a copy of the color in the desired format), or any of the setRgb(), setHsv() and setCmyk() functions altering this color's format.


The QPalette class contains color groups for each widget state. 

A palette consists of three color groups: Active, Disabled, and Inactive. All widgets in Qt contain a palette and use their palette to draw themselves. This makes the user interface easily configurable and easier to keep consistent.

The color groups:
-The Active group is used for the window that has keyboard focus.
-The Inactive group is used for other windows.
-The Disabled group is used for widgets (not windows) that are disabled for some reason.
Both active and inactive windows can contain disabled widgets.

To modify a color group you call the functions setColor() and setBrush(), depending on whether you want a pure color or a pixmap pattern.There are also corresponding color() and brush() getters. A color group contains a group of colors used by widgets for drawing themselves.



# Coordinate system / raster
* https://doc.qt.io/qt-6/coordsys.html
* https://doc.qt.io/qt-6/qrasterwindow.html


QRasterWindow is a convenience class for using QPainter on a QWindow.

QRasterWindow is a QWindow with a raster-based, non-OpenGL surface. On top of the functionality offered by QWindow, 
QRasterWindow adds a virtual paintEvent() function and the possibility to open a QPainter on itself. The underlying paint engine will be the raster one, meaning that all drawing will happen on the CPU.


The coordinate system is controlled by the QPainter class.

Together with the QPaintDevice and QPaintEngine classes, QPainter form the basis of Qt's painting system.
The default coordinate system of a paint device has its origin at the top-left corner. The x values increase to the right and the y values increase downwards. The size (width and height) of a graphics primitive always correspond to its mathematical model, ignoring the width of the pen it is rendered with:

When drawing, the pixel rendering is controlled by the QPainter::Antialiasing render hint.

The RenderHint enum is used to specify flags to QPainter that may or may not be respected by any given engine. The QPainter::Antialiasing value indicates that the engine should antialias edges of primitives if possible, i.e. smoothing the edges by using different color intensities.

When rendering with a pen with an even number of pixels, the pixels will be rendered symetrically around the mathematical defined points, while rendering with a pen with an odd number of pixels, the spare pixel will be rendered to the right and below the mathematical point as in the one pixel case.


(képek a vonalról és a téglalapról)

If you set QPainter's anti-aliasing render hint, the pixels will be rendered symetrically on both sides of the mathematically defined points:



# Drawing and Coloring Tools:
* https://doc.qt.io/qt-6/qbrush.html#details
* https://doc.qt.io/qt-5/qpen.html#details


The QPen class defines how a QPainter should draw lines and outlines of shapes.

A pen has a style(), width(), brush(), capStyle() and joinStyle().
The pen style defines the line type. The brush is used to fill strokes generated with the pen. Use the QBrush class to specify fill styles. 
The cap style determines the line end caps that can be drawn using QPainter, while the join style describes how joins between two lines are drawn.
Qt provides several built-in styles represented by the Qt::PenStyle enum:
(kép)
The cap style defines how the end points of lines are drawn using QPainter. The cap style only apply to wide lines, i.e. when the width is 1 or greater. 
The Qt::PenCapStyle enum provides the following styles:
(kép)
The join style defines how joins between two connected lines can be drawn using QPainter. The join style only apply to wide lines, i.e. when the width is 1 or greater. The Qt::PenJoinStyle enum provides the following styles:
(kép)


The QBrush class defines the fill pattern of shapes drawn by QPainter

The brush style() defines the fill pattern. The default brush style is Qt::NoBrush (depending on how you construct a brush). 
This style tells the painter to not fill shapes. The standard style for filling is Qt::SolidPattern. The style can be set when the brush is created using the appropriate constructor, and in addition the setStyle() function provides means for altering the style once the brush is constructed.
The brush color() defines the color of the fill pattern. The color can either be one of Qt's predefined colors, Qt::GlobalColor, or any other custom QColor. You can create a brush with a texture by providing the pixmap when the brush is created or by using setTexture().



# Images and text:
* https://doc.qt.io/qt-5/qfont.html#details
* https://doc.qt.io/qt-5/unicode.html
* https://doc.qt.io/qt-5/qimage.html


The QImage class provides a hardware-independent image representation that allows direct access to the pixel data, and can be used as a paint device.

QImage provides a collection of functions that can be used to obtain a variety of information about the image. (size(), width(), hasAlphaChannel(), textKeys(), depth(), format())QImage provides several ways of loading an image file: The file can be loaded when constructing the QImage object, or by using the load() or loadFromData() functions later on. QImage also provides the static fromData() function, constructing a QImage from the given data. Simply call the save() function to save a QImage object.
(kép)

There are also several functions that enables transformation of the image.
QImage supports a number of functions for creating a new image that is a transformed version of the original: The createAlphaMask() function builds and returns a 1-bpp mask from the alpha buffer in this image, and the createHeuristicMask() function creates and returns a 1-bpp heuristic mask for this image. The latter function works by selecting a color from one of the corners, then chipping away pixels of that color starting at all the edges.

The mirrored() function returns a mirror of the image in the desired direction, the scaled() returns a copy of the image scaled to a 
rectangle of the desired measures, and the rgbSwapped() function constructs a BGR image from a RGB image.

The scaledToWidth() and scaledToHeight() functions return scaled copies of the image.



In Qt, and in most applications that use Qt, most or all user-visible strings are stored using Unicode. Qt provides:

-Translation to/from legacy encodings for file I/O: see QTextCodec and QTextStream.
-Support for locale specific Input Methods and keyboards.
-A string class, QString, that stores Unicode characters, with support for migrating from C strings including fast translation to and from UTF-8, 
ISO8859-1 and US-ASCII, and all the usual string operations.
-Unicode-aware UI controls.
-Unicode compliant text segmentation (QTextBoundaryFinder)
-Unicode compliant line breaking and text rendering
To fully benefit from Unicode, we recommend using QString for storing all user-visible strings, and performing all text file I/O using QTextStream.


The QFont class specifies a query for a font used for drawing text.

When you create a QFont object you specify various attributes that you want the font to have. Qt will use the font with the specified attributes, 
or if no matching font exists, Qt will use the closest matching installed font.
The attributes of the font that is actually used are retrievable from a QFontInfo object.
To load a specific physical font, typically represented by a single file, use QRawFont instead.

If a chosen font does not include all the characters that need to be displayed, QFont will try to find the characters in the nearest equivalent fonts. 
When a QPainter draws a character from a font the QFont will report whether or not it has the character; if it does not, QPainter will draw an unfilled square.

The attributes set in the constructor can also be set later, e.g. setFamily(), setPointSize(), setPointSizeF(), setWeight() and setItalic(). 
The remaining attributes must be set after contstruction, e.g. setBold(), setUnderline(), setOverline(), setStrikeOut() and setFixedPitch(). 
QFontInfo objects should be created after the font's attributes have been set. 




