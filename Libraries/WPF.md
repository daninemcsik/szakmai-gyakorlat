# Bevezető:
* https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.visual?view=net-5.0
* https://docs.microsoft.com/en-us/dotnet/api/system.windows.uielement?view=net-5.0
* https://docs.microsoft.com/en-us/dotnet/desktop/wpf/advanced/wpf-architecture?view=netframeworkdesktop-4.8
* https://docs.microsoft.com/en-us/dotnet/api/system.windows.frameworkelement?view=net-5.0

Visual is really the entry point to the WPF composition system.
The Visual class provides for building a tree of visual objects, each optionally containing drawing instructions and metadata about how to render those instructions (clipping, transformation, etc.). 
Visual is designed to be extremely lightweight and flexible, so most of the features have no public API exposure and rely heavily on protected callback functions.The Visual class is the basic abstraction from which every FrameworkElement object derives.


Canvas class defines an area within which you can explicitly position child elements by using coordinates that 
are relative to the Canvas area.A Canvas contains a collection of UIElement objects, which are in the Children property.
Canvas is the only panel element that has no inherent layout characteristics. A Canvas has default Height and 
Width properties of zero, unless it is the child of an element that automatically sizes its child elements. 
Child elements of a Canvas are never resized, they are just positioned at their designated coordinates.


UIElement is a base class for WPF core level implementations building on Windows Presentation Foundation (WPF) 
elements and basic presentation characteristics. UIElement provides a starting point for element layout characteristics, and also exposes virtual methods that derived classes can override, which can influence the layout rendering behavior of the element and its child elements.
Much of the input and focusing behavior for elements in general is also defined in the UIElement class.
In terms of architecture, UIElement can be considered roughly equivalent to a window handle in Win32 programming, or an Element in Dynamic HTML (DHTML) programming. UIElement is a base element at the WPF core level.


FrameworkElement is the connecting point between WPF framework-level element classes and the WPF core-level set of UIElement presentation services. FrameworkElement extends UIElement and adds the following capabilities:
-Layout system definition: FrameworkElement provides specific WPF framework-level implementations for certain methods that were defined as virtual members in UIElement.
-The logical tree: The general WPF programming model is often expressed in terms of being a tree of elements.
-Object lifetime events: It is often useful to know when an element is initialized (the constructor is called) or when the element is first loaded into a logical tree.
-Styles: FrameworkElement defines the Style property. 
-More animation support: Some animation support was already defined at the WPF core level.



# Drawing:
* https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.visual?view=net-5.0
* https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.containervisual?view=net-5.0
* https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.drawingvisual?view=net-5.0
* https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.drawing?view=net-5.0


WPF uses a "painter's algorithm" painting model. This means that instead of clipping each component, 
each component is asked to render from the back to the front of the display. This allows each component 
to paint over the previous component's display. The advantage of this model is that you can have complex, 
partially transparent shapes.

The Visual object is a core WPF object, whose primary role is to provide rendering support. User interface 
controls, such as Button and TextBox, derive from the Visual class, and use the Visual defined properties 
for persisting their rendering data.

The ContainerVisual class is used as a container for a collection of Visual objects. The DrawingVisual class 
derives from the ContainerVisual class, such that the DrawingVisual class can also contain a collection of 
visual objects.

DrawingVisual is a visual object that can be used to render vector graphics on the screen. The content is 
persisted by the system.
DrawingVisual is a lightweight drawing class that is used to render shapes, images, or text. This class is 
considered lightweight because it does not provide layout, input, focus, or event handling, which improves 
its performance. For this reason, drawings are ideal for backgrounds and clip art.

Drawing class is an abstract class that describes a 2-D drawing.
Drawing objects are light-weight objects that enable you to add geometric shapes, images, text, and media to an application.

DrawingContext Class describes visual content using draw, push, and pop commands.
Use a DrawingContext to populate a Visual or a Drawing with visual content.
When you use a DrawingContext object's draw commands, you are actually storing a set of rendering instructions 
(although the exact storage mechanism depends on the type of object that supplies the DrawingContext) that 
will later be used by the graphics system; you are not drawing to the screen in real-time.



# Coloring:
* https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.color?view=net-5.0
* https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.colors?view=net-5.0


Color struct describes a color in terms of alpha, red, green, and blue channels.

Colors class impelemnts a set of predefined colors.
The Windows Presentation Foundation color names match the .NET Framework, Windows Forms, and Microsoft Internet Explorer color names. This representation is based on UNIX X11 named color values.
The following image shows each predefined color, its name, and its hexadecimal value.
(kép)
Color Table including a color swatch, the color name, and the hexadecimal value.



# Coordinate system / raster / vector system
* https://docs.microsoft.com/en-us/dotnet/desktop/wpf/graphics-multimedia/wpf-graphics-rendering-overview?view=netframeworkdesktop-4.8


A user interface element, such as a Button control, contains several vector graphics instruction lists that describe 
the entire rendering definition of a control.

If you were to enumerate the visual objects and vector graphics instruction lists that comprise the Button control, you would find the hierarchy of objects illustrated below:
(kép)
The Button control contains a ClassicBorderDecorator element, which in turn, contains a ContentPresenter element. The ClassicBorderDecorator element is responsible for drawing all the discrete graphic elements that make up the border and background of a button. The ContentPresenter element is responsible for displaying the contents of the Button. In this case, since you are displaying an image, the ContentPresenter element contains a Image element.

There are a number of points to note about the hierarchy of visual objects and vector graphics instruction lists:
-The ordering in the hierarchy represents the rendering order of the drawing information. From the root visual element, child elements are traversed, left to right, top to bottom. If an element has visual child elements, they are traversed before the element’s siblings.
-Non-leaf node elements in the hierarchy, such as ContentPresenter, are used to contain child elements—they do not contain instruction lists.
-If a visual element contains both a vector graphics instruction list and visual children, the instruction list in the parent visual element is rendered before drawings in any of the visual child objects.
-The items in the vector graphics instruction list are rendered left to right.

WPF uses vector graphics as its rendering data format. Vector graphics—which include Scalable Vector Graphics (SVG), 
Windows metafiles (.wmf), and TrueType fonts—store rendering data and transmit it as a list of instructions that 
describe how to recreate an image using graphics primitives. For example, TrueType fonts are outline fonts that 
describe a set of lines, curves, and commands, rather than an array of pixels. One of the key benefits of vector 
graphics is the ability to scale to any size and resolution.

Unlike vector graphics, bitmap graphics store rendering data as a pixel-by-pixel representation of an image, 
pre-rendered for a specific resolution. One of the key differences between bitmap and vector graphic formats is 
fidelity to the original source image. For example, when the size of a source image is modified, bitmap graphics 
systems stretch the image, whereas vector graphics systems scale the image, preserving the image fidelity.



# Drawing and coloring tools:
* https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.brush?view=net-5.0
* https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.drawingbrush?view=net-5.0


Brush class defines objects used to paint graphical objects.
A Brush "paints" or "fills" an area with its output. Different brushes have different types of output. 
Some brushes paint an area with a solid color, others with a gradient, pattern, image, or drawing. 
The following list describes the different types of WPF brushes:
-SolidColorBrush: Paints an area with a solid Color.
-LinearGradientBrush: Paints an area with a linear gradient.
-RadialGradientBrush: Paints an area with a radial gradient.
-ImageBrush: Paints an area with an image (represented by an ImageSource object).
-DrawingBrush: Paints an area with a Drawing. The drawing may include vector and bitmap objects.
-VisualBrush: Paints an area with a Visual object. A VisualBrush enables you to duplicate content from one 
portion of your application into another area; it's very useful for creating reflection effects and magnifying 
portions of the screen.

DrawingBrush Class paints an area with a Drawing, which can include shapes, text, video, images, or other drawings.


Pen class describes how a shape is outlined.

PenDashCap Enum describes the shape at the end of each dash segment.
PenLineCap Enum describes the shape at the end of a line or segment.
PenLineJoin Enum describes the shape that joins two lines or segments.



# Images and text:
https://docs.microsoft.com/en-us/dotnet/api/system.windows.controls.image?view=net-5.0
https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.imagedrawing?view=net-5.0


A font family is a set of typefaces that share the same family name, such as "Times New Roman", but that 
differ in features. These feature differences include Style, such as italic, and Weight, such as bold.

Most user interface (UI) elements, such as Button and TextBlock, provide a FontFamily property that can be 
used to specify a font for the text content of a control. You define the font by setting that property with a 
FontFamily value.

GlyphRun class represents a sequence of glyphs from a single face of a single font at a single size, and with a single rendering style.
The GlyphRun object includes font details such as glyph indices and individual glyph positions. In addition, 
The GlyphRun object contains the original Unicode code points the run was generated from, character to glyph 
buffer offset mapping information, and per-character and per-glyph flags.


Image class represents a control that displays an image.
The Image class enables you to load the following image types: .bmp, .gif, .ico, .jpg, .png, .wdp, and .tiff.
When displaying a multiframe image, only the first frame is displayed. The animation of multiframe images is 
not supported by the Image control.
Until the image content is loaded, the ActualWidth and ActualHeight of the control will report as zero, 
because the image content is used to determine the final size and location of the control.
For a fixed size control, the Width and/or Height properties can be set. However, to preserve the media's 
aspect ratio, set the Width or Height properties but not both.

ImageDrawing class draws an image within a region defined by a Rect.
