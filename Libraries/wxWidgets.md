# Bevezető:
* https://docs.wxwidgets.org/trunk/classwx_window.html
* https://docs.wxwidgets.org/trunk/classwx_control.html
* https://docs.wxwidgets.org/trunk/classwx_panel.html



wxWindow is the base class for all windows and represents any visible object on screen.
All controls, top level windows and so on are windows. Sizers and device contexts are not, however, as they don't appear on screen themselves.

wxControl is the base class for a control or "widget".
A control is generally a small window which processes user input and/or displays one or more item of data.

wxPanel class - A panel is a window on which controls are placed.
It is usually placed within a frame. Its main feature over its parent class wxWindow is code for handling child windows and TAB traversal, which is implemented natively if possible (e.g. in wxGTK) or by wxWidgets itself otherwise.

wxTopLevelWindow is a common base class for wxDialog and wxFrame.
It is an abstract base class meaning that you never work with objects of this class directly, but all of its methods are also applicable for the two classes above.
Note that the instances of wxTopLevelWindow are managed by wxWidgets in the internal top level window list.



# Drawing:
* https://docs.wxwidgets.org/trunk/overview_dc.html
* https://docs.wxwidgets.org/trunk/classwx_d_c.html
* https://docs.wxwidgets.org/trunk/classwx_screen_d_c.html
* https://docs.wxwidgets.org/trunk/classwx_client_d_c.html
* https://docs.wxwidgets.org/trunk/classwx_paint_d_c.html
* https://docs.wxwidgets.org/trunk/classwx_window_d_c.html
* https://docs.wxwidgets.org/trunk/classwx_memory_d_c.html
* https://docs.wxwidgets.org/trunk/classwx_buffered_d_c.html
* https://docs.wxwidgets.org/trunk/classwx_buffered_paint_d_c.html
* https://docs.wxwidgets.org/trunk/classwx_auto_buffered_paint_d_c.html



A wxDC is a device context onto which graphics and text can be drawn.
Some device contexts are created temporarily in order to draw on a window. 
This is true of wxScreenDC, wxClientDC, wxPaintDC, and wxWindowDC.

-wxScreenDC : Use this to paint on the screen, as opposed to an individual window.
A wxScreenDC can be used to paint on the screen.
This should normally be constructed as a temporary stack object; don't store a wxScreenDC object.

-wxClientDC : Use this to paint on the client area of window (the part without borders and other decorations).
wxClientDC objects should normally be constructed as temporary stack objects, i.e. don't store a wxClientDC object.
A wxClientDC object is initialized to use the same font and colours as the window it is associated with.

-wxPaintDC : Use this to paint on the client area of a window,
A wxPaintDC must be constructed if an application wishes to paint on the client area of a window from within an EVT_PAINT() event handler.
This should normally be constructed as a temporary stack object; don't store a wxPaintDC object. If you have an EVT_PAINT() handler, you must create a wxPaintDC object within it even if you don't actually use it.
Using wxPaintDC within your EVT_PAINT() handler is important because it automatically sets the clipping area to the damaged area of the window. Attempts to draw outside this area do not appear.
A wxPaintDC object is initialized to use the same font and colours as the window it is associated with.

-wxWindowDC: Use this to paint on the whole area of a window, including decorations. This may not be available on non-Windows platforms.
A wxWindowDC must be constructed if an application wishes to paint on the whole area of a window (client and decorations).
This should normally be constructed as a temporary stack object; don't store a wxWindowDC object.
To draw on a window from inside an EVT_PAINT() handler, construct a wxPaintDC object instead.
To draw on the client area of a window from outside an EVT_PAINT() handler, construct a wxClientDC object.
A wxWindowDC object is initialized to use the same font and colours as the window it is associated with.

wxMemoryDC class:
A memory device context provides a means to draw graphics onto a bitmap.
When drawing in to a mono-bitmap, using wxWHITE, wxWHITE_PEN and wxWHITE_BRUSH will draw the background colour whereas all other colours will draw the foreground colour. A bitmap must be selected into the new memory DC before it may be used for anything.

wxBufferedDC class provides a simple way to avoid flicker: when drawing on it, everything is in fact first drawn on an in-memory buffer (a wxBitmap) and then copied to the screen, using the associated wxDC, only once, when this object is destroyed.
wxBufferedDC itself is typically associated with wxClientDC.
When used like this, a valid DC must be specified in the constructor while the buffer bitmap doesn't have to be explicitly provided, by default this class will allocate the bitmap of required size itself. However using a dedicated bitmap can speed up the redrawing process by eliminating the repeated creation and destruction of a possibly big bitmap. Otherwise, wxBufferedDC can be used in the same way as any other device context.
Another possible use for wxBufferedDC is to use it to maintain a backing store for the window contents.

wxBufferecPaintDC class is a subclass of wxBufferedDC which can be used inside of an EVT_PAINT() event handler to achieve double-buffered drawing.

wxAutoBufferedPaintDC is a wxDC derivative can be used inside of an EVT_PAINT() event handler to achieve double-buffered drawing.
The difference between wxBufferedPaintDC and this class is that this class won't double-buffer on platforms which have native double-buffering already, avoiding any unnecessary buffering to avoid flicker.
wxAutoBufferedPaintDC is simply a typedef of wxPaintDC on platforms that have native double-buffering, otherwise, it is a typedef of wxBufferedPaintDC.



# Coloring:
* https://docs.wxwidgets.org/trunk/classwx_colour.html
* https://docs.wxwidgets.org/trunk/classwx_colour_database.html
* https://docs.wxwidgets.org/trunk/classwx_palette.html



wxColour class: 
A colour is an object representing a combination of Red, Green, and Blue (RGB) intensity values and an Alpha value, and is used to determine drawing colours. Valid RGB values are in the range 0 to 255. You can retrieve the current system colour settings with wxSystemSettings.

wxColourDatabase class: 
wxWidgets maintains a database of standard RGB colours for a predefined set of named colours.
The application may add to this set if desired by using AddColour() and may use it to look up colours by names using Find() or find the names for the standard colour using FindName().
(kép)

wxPalette class: 
A palette is a table that maps pixel values to RGB colours.
It allows the colours of a low-depth bitmap, for example, to be mapped to the available colours in a display. The notion of palettes is becoming more and more obsolete nowadays and only the MSW port is still using a native palette. All other ports use generic code which is basically just an array of colours.
It is likely that in the future the only use for palettes within wxWidgets will be for representing colour indices from images (such as GIF or PNG). The image handlers for these formats have been modified to create a palette if there is such information in the original image file (usually 256 or less colour images).



# raster/vector:
* https://docs.wxwidgets.org/trunk/classwx_graphics_renderer.html
* https://docs.wxwidgets.org/trunk/classwx_graphics_context.html
* https://docs.wxwidgets.org/trunk/classwx_renderer_native.html



A wxGraphicsRenderer is the instance corresponding to the rendering engine used.
There may be multiple instances on a system, if there are different rendering engines present, but there is always only one instance per engine. This instance is pointed back to by all objects created by it (wxGraphicsContext, wxGraphicsPath etc.) and can be retrieved through their wxGraphicsObject::GetRenderer() method.

A wxGraphicsContext instance is the object that is drawn upon.
It is created by a renderer using wxGraphicsRenderer::CreateContext(). This can be either directly using a renderer instance, or indirectly using the static convenience Create() functions of wxGraphicsContext that always delegate the task to the default renderer.

wxRendererNative class:
Usually wxWidgets uses the underlying low level GUI system to draw all the controls - this is what we mean when we say that it is a "native" framework. However not all controls exist under all (or even any) platforms and in this case wxWidgets provides a default, generic, implementation of them written in wxWidgets itself.
These controls don't have the native appearance if only the standard line drawing and other graphics primitives are used, because the native appearance is different under different platforms while the lines are always drawn in the same way.
This is why we have renderers: wxRendererNative is a class which virtualizes the drawing, i.e. it abstracts the drawing operations and allows you to draw say, a button, without caring about exactly how this is done. Of course, as we can draw the button differently in different renderers, this also allows us to emulate the native look and feel.
So the renderers work by exposing a large set of high-level drawing functions which are used by the generic controls.

All drawing functions take some standard parameters:
-win - The window being drawn. It is normally not used and when it is it should only be used as a generic wxWindow, but you should not assume that it is of some given type as the same renderer function may be reused for drawing different kinds of control.
-dc - The wxDC to draw on. Only this device context should be used for drawing. It is not necessary to restore pens and brushes for it on function exit but, on the other hand, you shouldn't assume that it is in any specific state on function entry: the rendering functions should always prepare it.
-rect - The bounding rectangle for the element to be drawn.
-flags - The optional flags (none by default) which can be a combination of the wxCONTROL_FLAGS.
Note that each drawing function restores the wxDC attributes if it changes them, so it is safe to assume that the same pen, brush and colours that were active before the call to this function are still in effect after it.



# Drawing and coloring tools:
* https://docs.wxwidgets.org/trunk/classwx_brush.html
* https://docs.wxwidgets.org/trunk/classwx_brush_list.html
* https://docs.wxwidgets.org/trunk/classwx_graphics_brush.html
* https://docs.wxwidgets.org/trunk/classwx_pen.html
* https://docs.wxwidgets.org/trunk/classwx_pen_list.html
* https://docs.wxwidgets.org/trunk/classwx_graphics_pen.html



wxBrush class: 
A brush is a drawing tool for filling in areas.
It is used for painting the background of rectangles, ellipses, etc. It has a colour and a style.
On a monochrome display, wxWidgets shows all brushes as white unless the colour is really black.
An application may wish to create brushes with different characteristics dynamically, and there is the consequent danger that a large number of duplicate brushes will be created. Therefore an application may wish to get a pointer to a brush by using the global list of brushes wxTheBrushList, and calling the member function wxBrushList::FindOrCreateBrush().

wxBrushList class: 
A brush list is a list containing all brushes which have been created.
The application should not construct its own brush list: it should use the object pointer wxTheBrushList.

A wxGraphicsBrush is a native representation of a brush.
The contents are specific and private to the respective renderer. Instances are ref counted and can therefore be assigned as usual. 
The only way to get a valid instance is via wxGraphicsContext::CreateBrush() or wxGraphicsRenderer::CreateBrush().


wxPen class: 
A pen is a drawing tool for drawing outlines.
It is used for drawing lines and painting the outline of rectangles, ellipses, etc. It has a colour, a width and a style.
An application may wish to dynamically create pens with different characteristics, and there is the consequent danger that a large number of duplicate pens will be created. Therefore an application may wish to get a pointer to a pen by using the global list of pens wxThePenList, and calling the member function wxPenList::FindOrCreatePen().

wxPenList class: 
There is only one instance of this class: wxThePenList.
Use this object to search for a previously created pen of the desired type and create it if not already found. In some windowing systems, the pen may be a scarce resource, so it can pay to reuse old resources if possible. When an application finishes, all pens will be deleted and their resources freed, eliminating the possibility of 'memory leaks'.

A wxGraphicsPen is a native representation of a pen.
The contents are specific and private to the respective renderer. Instances are ref counted and can therefore be assigned as usual. The only way to get a valid instance is via wxGraphicsContext::CreatePen() or wxGraphicsRenderer::CreatePen().



# Images and text:
* https://docs.wxwidgets.org/trunk/classwx_font.html
* https://docs.wxwidgets.org/trunk/classwx_font_info.html
* https://docs.wxwidgets.org/trunk/classwx_font_list.html
* https://docs.wxwidgets.org/trunk/classwx_graphics_font.html
* https://docs.wxwidgets.org/trunk/classwx_bitmap.html
* https://docs.wxwidgets.org/trunk/classwx_image.html
* https://docs.wxwidgets.org/trunk/classwx_image_list.html



wxFont class: 
A font is an object which determines the appearance of text.
Fonts are used for drawing text to a device context, and setting the appearance of a window's text, see wxDC::SetFont() and wxWindow::SetFont().
The easiest way to create a custom font is to use wxFontInfo object to specify the font attributes and then use 
wxFont::wxFont(const wxFontInfo&) constructor.

wxFontInfo class: 
This class is a helper used for wxFont creation using named parameter idiom: it allows specifying various wxFont attributes using the chained calls to its clearly named methods instead of passing them in the fixed order to wxFont constructors.

wxFontList class: 
A font list is a list containing all fonts which have been created.
There is only one instance of this class: wxTheFontList.
Use this object to search for a previously created font of the desired type and create it if not already found.
When an application finishes, all fonts will be deleted and their resources freed, eliminating the possibility of 'memory leaks'.

A wxGraphicsFont is a native representation of a font.
The contents are specific and private to the respective renderer. Instances are ref counted and can therefore be assigned as usual. The only way to get a valid instance is via wxGraphicsContext::CreateFont() or wxGraphicsRenderer::CreateFont().


wxBitmap class: 
This class encapsulates the concept of a platform-dependent bitmap, either monochrome or colour or colour with alpha channel support.

wxImage class: 
This class encapsulates a platform-independent image.
An image can be created from data, or using wxBitmap::ConvertToImage. An image can be loaded from a file in a variety of formats, and is extensible to new formats via image format handlers. Functions are available to set and get image bits, so it can be used for basic image manipulation.

A wxImage cannot (currently) be drawn directly to a wxDC. Instead, a platform-specific wxBitmap object must be created from it using the wxBitmap::wxBitmap(wxImage,int depth) constructor. This bitmap can then be drawn in a device context, using wxDC::DrawBitmap.
More on the difference between wxImage and wxBitmap: wxImage is just a buffer of RGB bytes with an optional buffer for the alpha bytes. 
It is all generic, platform independent and image file format independent code. It includes generic code for scaling, resizing, clipping, and other manipulations of the image data.

Starting from wxWidgets 2.5.0 wxImage supports alpha channel data, that is in addition to a byte for the red, green and blue colour components for each pixel it also stores a byte representing the pixel opacity. An alpha value of 0 corresponds to a transparent pixel (null opacity) while a value of 255 means that the pixel is 100% opaque.

The following image handlers are available. wxBMPHandler is always installed by default. To use other image formats, install the appropriate handler with wxImage::AddHandler or call wxInitAllImageHandlers().

-wxBMPHandler: For loading (including alpha support) and saving, always installed.
-wxPNGHandler: For loading and saving. Includes alpha support.
-wxJPEGHandler: For loading and saving.
-wxGIFHandler: For loading and saving (see below).
-wxPCXHandler: For loading and saving (see below).
-wxPNMHandler: For loading and saving (see below).
-wxTIFFHandler: For loading and saving. Includes alpha support.
-wxTGAHandler: For loading and saving. Includes alpha support.
-wxIFFHandler: For loading only.
-wxXPMHandler: For loading and saving.
-wxICOHandler: For loading and saving.
-wxCURHandler: For loading and saving.
-wxANIHandler: For loading only.

wxImageList class:
A wxImageList contains a list of images, which are stored in an unspecified form.
Images can have masks for transparent drawing, and can be made from a variety of sources including bitmaps and icons.
