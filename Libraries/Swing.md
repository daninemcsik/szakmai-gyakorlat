# Bevezető:
* https://docs.oracle.com/javase/7/docs/api/java/awt/Component.html
* https://docs.oracle.com/javase/7/docs/api/javax/swing/JComponent.html
* https://docs.oracle.com/javase/7/docs/api/java/awt/Container.html
* https://docs.oracle.com/javase/7/docs/api/java/awt/Panel.html
* https://docs.oracle.com/javase/7/docs/api/javax/swing/JPanel.html
* https://docs.oracle.com/javase/7/docs/api/java/awt/Canvas.html
* https://docs.oracle.com/javase/7/docs/api/java/awt/Window.html
* https://docs.oracle.com/javase/7/docs/api/javax/swing/JWindow.html
* https://docs.oracle.com/javase/7/docs/api/java/awt/Frame.html
* https://docs.oracle.com/javase/7/docs/api/javax/swing/JFrame.html
* https://docs.oracle.com/javase/7/docs/api/javax/swing/JInternalFrame.html
* https://docs.oracle.com/javase/7/docs/api/javax/swing/JDesktopPane.html

Component class: 
A component is an object having a graphical representation that can be displayed on the screen and that can interact with the user. 
Examples of components are the buttons, checkboxes, and scrollbars of a typical graphical user interface.
The Component class is the abstract superclass of the nonmenu-related Abstract Window Toolkit components. Class Component can also be extended directly to create a lightweight component. A lightweight component is a component that is not associated with a native window. On the contrary, a heavyweight component is associated with a native window. 

JComponent class:
The base class for all Swing components except top-level containers. To use a component that inherits from JComponent, you must place the component in a containment hierarchy whose root is a top-level Swing container. Top-level Swing containers -- such as JFrame, JDialog, and JApplet -- are specialized components that provide a place for other Swing components to paint themselves.

Container class: 
A generic Abstract Window Toolkit(AWT) container object is a component that can contain other AWT components.
Components added to a container are tracked in a list. The order of the list will define the components' front-to-back stacking order within the container. If no index is specified when adding a component to a container, it will be added to the end of the list.

Panel class:
Panel is the simplest container class. A panel provides space in which an application can attach any other component, including other panels. The default layout manager for a panel is the FlowLayout layout manager.

JPanel class:
JPanel is a generic lightweight container.

Canvas class: 
A Canvas component represents a blank rectangular area of the screen onto which the application can draw or from which the application can trap input events from the user.
An application must subclass the Canvas class in order to get useful functionality such as creating a custom component. The paint method must be overridden in order to perform custom graphics on the canvas.

Window class:
A Window object is a top-level window with no borders and no menubar.
A window must have either a frame, dialog, or another window defined as its owner when it's constructed.

JWindow class:
A JWindow is a container that can be displayed anywhere on the user's desktop. It does not have the title bar, window-management buttons, or other trimmings associated with a JFrame, but it is still a "first-class citizen" of the user's desktop, and can exist anywhere on it.
The JWindow component contains a JRootPane as its only child. The contentPane should be the parent of any children of the JWindow. As a conveniance add and its variants, remove and setLayout have been overridden to forward to the contentPane as necessary.

Frame class:
A Frame is a top-level window with a title and a border. The default layout for a frame is BorderLayout.
A frame may have its native decorations (i.e. Frame and Titlebar) turned off with setUndecorated. This can only be done while the frame is not displayable.

JFrame class:
An extended version of java.awt.Frame that adds support for the JFC/Swing component architecture.
The JFrame class is slightly incompatible with Frame. Like all other JFC/Swing top-level containers, a JFrame contains a JRootPane as its only child. 
The content pane provided by the root pane should, as a rule, contain all the non-menu components displayed by the JFrame. This is different from the AWT Frame case. As a conveniance add and its variants, remove and setLayout have been overridden to forward to the contentPane as necessary.
Unlike a Frame, a JFrame has some notion of how to respond when the user attempts to close the window. The default behavior is to simply hide the JFrame when the user closes the window.

JDesktopPane class:
A container used to create a multiple-document interface or a virtual desktop. You create JInternalFrame objects and add them to the JDesktopPane. 
JDesktopPane extends JLayeredPane to manage the potentially overlapping internal frames. It also maintains a reference to an instance of DesktopManager that is set by the UI class for the current look and feel (L&F). Note that JDesktopPane does not support borders.
This class is normally used as the parent of JInternalFrames to provide a pluggable DesktopManager object to the JInternalFrames.

JInternalFrame class:
A lightweight object that provides many of the features of a native frame, including dragging, closing, becoming an icon, resizing, title display, and support for a menu bar.
Generally, you add JInternalFrames to a JDesktopPane. The UI delegates the look-and-feel-specific actions to the DesktopManager object maintained by the JDesktopPane.
The JInternalFrame content pane is where you add child components. As a conveniance add and its variants, remove and setLayout have been overridden to forward to the contentPane as necessary.



# Drawing:
* https://docs.oracle.com/javase/7/docs/api/java/awt/Graphics.html
* https://docs.oracle.com/javase/7/docs/api/java/awt/Graphics2D.html


Graphics class:
The Graphics class is the abstract base class for all graphics contexts that allow an application to draw onto components that are realized on various devices, as well as onto off-screen images.
A Graphics object encapsulates state information needed for the basic rendering operations that Java supports. This state information includes the following properties:
-The Component object on which to draw.
-A translation origin for rendering and clipping coordinates.
-The current clip.
-The current color.
-The current font.
-The current logical pixel operation function.
-The current XOR alternation color.
Coordinates are infinitely thin and lie between the pixels of the output device. Operations that draw the outline of a figure operate by traversing an infinitely thin path between pixels with a pixel-sized pen that hangs down and to the right of the anchor point on the path. Operations that fill a figure operate by filling the interior of that infinitely thin path. Operations that render horizontal text render the ascending portion of character glyphs entirely above the baseline coordinate.


Graphics2D class:
This Graphics2D class extends the Graphics class to provide more sophisticated control over geometry, coordinate transformations, color management, and text layout. This is the fundamental class for rendering 2-dimensional shapes, text and images on the Java(tm) platform.

-Coordinate Spaces: 
All coordinates passed to a Graphics2D object are specified in a device-independent coordinate system called User Space, which is used by applications.
The Graphics2D object contains an AffineTransform object as part of its rendering state that defines how to convert coordinates from user space to device-dependent coordinates in Device Space.
Some of the operations performed by the rendering attribute objects occur in the device space, but all Graphics2D methods take user space coordinates.

-Rendering process: 
The Rendering Process can be broken down into four phases that are controlled by the Graphics2D rendering attributes:
	-Determine what to render.
	-Constrain the rendering operation to the current Clip. The Clip is specified by a Shape in user space and is controlled by the program using the 
various clip manipulation methods of Graphics and Graphics2D. This user clip is transformed into device space by the current Transform and combined with the device clip, which is defined by the visibility of windows and device extents. The combination of the user clip and device clip defines the composite clip, which determines the final clipping region. The user clip is not modified by the rendering system to reflect the resulting composite clip.
	-Determine what colors to render.
	-Apply the colors to the destination drawing surface using the current Composite attribute in the Graphics2D context.

The three types of rendering operations, along with details of each of their particular rendering processes are:
-Shape operations:
	-If the operation is a draw(Shape) operation, then the createStrokedShape method on the current Stroke attribute in the Graphics2D context is used 
	to construct a new Shape object that contains the outline of the specified Shape.
	-The Shape is transformed from user space to device space using the current Transform in the Graphics2D context.
	-The outline of the Shape is extracted using the getPathIterator method of Shape, which returns a PathIterator object that iterates along the boundary 
	of the Shape.
	-If the Graphics2D object cannot handle the curved segments that the PathIterator object returns then it can call the alternate getPathIterator method 
	of Shape, which flattens the Shape.
	-The current Paint in the Graphics2D context is queried for a PaintContext, which specifies the colors to render in device space.
-Text operations:
	-The following steps are used to determine the set of glyphs required to render the indicated String:
		-If the argument is a String, then the current Font in the Graphics2D context is asked to convert the Unicode characters in the String into a set of glyphs for presentation with whatever basic layout and shaping algorithms the font implements.
		-If the argument is an AttributedCharacterIterator, the iterator is asked to convert itself to a TextLayout using its embedded font attributes. The extLayout implements more sophisticated glyph layout algorithms that perform Unicode bi-directional layout adjustments automatically for multiple 
		fonts of differing writing directions.
		-If the argument is a GlyphVector, then the GlyphVector object already contains the appropriate font-specific glyph codes with explicit coordinates for the position of each glyph.
	-The current Font is queried to obtain outlines for the indicated glyphs. These outlines are treated as shapes in user space relative to the position each glyph that was determined in step 1.
	-The character outlines are filled as indicated above under Shape operations.
	-The current Paint is queried for a PaintContext, which specifies the colors to render in device space.
-Image Operations:
	-The region of interest is defined by the bounding box of the source Image. This bounding box is specified in Image Space, which is the Image object's local coordinate system.
	-If an AffineTransform is passed to drawImage(Image, AffineTransform, ImageObserver), the AffineTransform is used to transform the bounding box from image space to user space. If no AffineTransform is supplied, the bounding box is treated as if it is already in user space.
	-The bounding box of the source Image is transformed from user space into device space using the current Transform. Note that the result of transforming the bounding box does not necessarily result in a rectangular region in device space.
	-The Image object determines what colors to render, sampled according to the source to destination coordinate mapping specified by the current Transform and the optional image transform.



# Coloring:
* https://docs.oracle.com/javase/7/docs/api/java/awt/Color.html
* https://docs.oracle.com/javase/7/docs/api/java/awt/SystemColor.html


Color class:
The Color class is used to encapsulate colors in the default sRGB color space or colors in arbitrary color spaces identified by a ColorSpace. Every color has an implicit alpha value of 1.0 or an explicit one provided in the constructor. The alpha value defines the transparency of a color and can be represented by a float value in the range 0.0 - 1.0 or 0 - 255. An alpha value of 1.0 or 255 means that the color is completely opaque and an alpha value of 0 or 0.0 means that the color is completely transparent. When constructing a Color with an explicit alpha or getting the color/alpha components of a Color, the color components are never premultiplied by the alpha component.

SystemColor class:
A class to encapsulate symbolic colors representing the color of native GUI objects on a system. For systems which support the dynamic update of the system colors (when the user changes the colors) the actual RGB values of these symbolic colors will also change dynamically. In order to compare the "current" RGB value of a SystemColor object with a non-symbolic Color object, getRGB should be used rather than equals.
Note that the way in which these system colors are applied to GUI objects may vary slightly from platform to platform since GUI objects may be rendered differently on each platform.
System color values may also be available through the getDesktopProperty method on java.awt.Toolkit.



# Drawing and Coloring tools:
* https://docs.oracle.com/javase/7/docs/api/java/awt/BasicStroke.html


BasicStroke class:

The BasicStroke class defines a basic set of rendering attributes for the outlines of graphics primitives, which are rendered with a Graphics2D object that has its Stroke attribute set to this BasicStroke. The rendering attributes defined by BasicStroke describe the shape of the mark made by a pen drawn along the outline of a Shape and the decorations applied at the ends and joins of path segments of the Shape. These rendering attributes include:
-width:
The pen width, measured perpendicularly to the pen trajectory.
-end caps:
The decoration applied to the ends of unclosed subpaths and dash segments. Subpaths that start and end on the same point are still considered unclosed if 
they do not have a CLOSE segment. See SEG_CLOSE for more information on the CLOSE segment. The three different decorations are: CAP_BUTT, CAP_ROUND, and CAP_SQUARE.
-line joins:
The decoration applied at the intersection of two path segments and at the intersection of the endpoints of a subpath that is closed using SEG_CLOSE. The three different decorations are: JOIN_BEVEL, JOIN_MITER, and JOIN_ROUND.
-miter limit:
The limit to trim a line join that has a JOIN_MITER decoration. A line join is trimmed when the ratio of miter length to stroke width is greater than the miterlimit value. 
-dash attributes:
The definition of how to make a dash pattern by alternating between opaque and transparent sections.

All attributes that specify measurements and distances controlling the shape of the returned outline are measured in the same coordinate system as the original unstroked Shape argument. When a Graphics2D object uses a Stroke object to redefine a path during the execution of one of its draw methods, the geometry is supplied in its original form before the Graphics2D transform attribute is applied. Therefore, attributes such as the pen width are interpreted in the user 
space coordinate system of the Graphics2D object and are subject to the scaling and shearing effects of the user-space-to-device-space transform in that particular Graphics2D.



# Raster / vector graphics:
* https://docs.oracle.com/javase/7/docs/api/java/awt/image/Raster.html


A class representing a rectangular array of pixels. A Raster encapsulates a DataBuffer that stores the sample values and a SampleModel that describes how to 
locate a given sample value in a DataBuffer.
A Raster defines values for pixels occupying a particular rectangular area of the plane, not necessarily including (0, 0). The rectangle, known as the Raster's bounding rectangle and available by means of the getBounds method, is defined by minX, minY, width, and height values. The minX and minY values define the coordinate of the upper left corner of the Raster. References to pixels outside of the bounding rectangle may result in an exception being thrown, or may result in references to unintended elements of the Raster's associated DataBuffer. It is the user's responsibility to avoid accessing such pixels.

A SampleModel describes how samples of a Raster are stored in the primitive array elements of a DataBuffer. Samples may be stored one per data element, as in a PixelInterleavedSampleModel or BandedSampleModel, or packed several to an element, as in a SinglePixelPackedSampleModel or MultiPixelPackedSampleModel. 
The SampleModel is also controls whether samples are sign extended, allowing unsigned data to be stored in signed Java data types such as byte, short, and int.

Although a Raster may live anywhere in the plane, a SampleModel makes use of a simple coordinate system that starts at (0, 0). A Raster therefore contains a translation factor that allows pixel locations to be mapped between the Raster's coordinate system and that of the SampleModel. The translation from the SampleModel coordinate system to that of the Raster may be obtained by the getSampleModelTranslateX and getSampleModelTranslateY methods.

A Raster may share a DataBuffer with another Raster either by explicit construction or by the use of the createChild and createTranslatedChild methods. Rasters created by these methods can return a reference to the Raster they were created from by means of the getParent method. For a Raster that was not constructed by means of a call to createTranslatedChild or createChild, getParent will return null.

The createTranslatedChild method returns a new Raster that shares all of the data of the current Raster, but occupies a bounding rectangle of the same width and height but with a different starting point. For example, if the parent Raster occupied the region (10, 10) to (100, 100), and the translated Raster was defined to start at (50, 50), then pixel (20, 20) of the parent and pixel (60, 60) of the child occupy the same location in the DataBuffer shared by the two Rasters. In the first case, (-10, -10) should be added to a pixel coordinate to obtain the corresponding SampleModel coordinate, and in the second case (-50, -50) should be added.

The translation between a parent and child Raster may be determined by subtracting the child's sampleModelTranslateX and sampleModelTranslateY values from those of the parent.

The createChild method may be used to create a new Raster occupying only a subset of its parent's bounding rectangle (with the same or a translated coordinate system) or with a subset of the bands of its parent.

All constructors are protected. The correct way to create a Raster is to use one of the static create methods defined in this class. These methods create instances of Raster that use the standard Interleaved, Banded, and Packed SampleModels and that may be processed more efficiently than a Raster created by combining an externally generated SampleModel and DataBuffer.




# Images and text:
* https://docs.oracle.com/javase/7/docs/api/java/awt/Font.html
* https://docs.oracle.com/javase/7/docs/api/java/awt/Image.html
* https://docs.oracle.com/javase/7/docs/api/java/awt/image/BufferedImage.html
* https://docs.oracle.com/javase/7/docs/api/java/awt/image/VolatileImage.html
* https://docs.oracle.com/javase/7/docs/api/java/awt/TexturePaint.html


Font class:
The Font class represents fonts, which are used to render text in a visible way. A font provides the information needed to map sequences of characters to sequences of glyphs and to render sequences of glyphs on Graphics and Component objects.
-Characters and Glyphs: 
A character is a symbol that represents an item such as a letter, a digit, or punctuation in an abstract way.
A glyph is a shape used to render a character or a sequence of characters. In simple writing systems, such as Latin, typically one glyph represents one character. In general, however, characters and glyphs do not have one-to-one correspondence. For example, the character 'á' LATIN SMALL LETTER A WITH ACUTE, can be represented by two glyphs: one for 'a' and one for '´'.
 
-Physical and Logical Fonts: 
The Java Platform distinguishes between two kinds of fonts: physical fonts and logical fonts.
Physical fonts are the actual font libraries containing glyph data and tables to map from character sequences to glyph sequences, using a font technology such as TrueType or PostScript Type 1.
All implementations of the Java Platform must support TrueType fonts; support for other font technologies is implementation dependent.
Logical fonts are the five font families defined by the Java platform which must be supported by any Java runtime environment: Serif, SansSerif, Monospaced, Dialog, and DialogInput. These logical fonts are not actual font libraries. Instead, the logical font names are mapped to physical fonts by the Java runtime environment. 
Peered AWT components, such as Label and TextField, can only use logical fonts.

-Font Faces and Names: 
A Font can have many faces, such as heavy, medium, oblique, gothic and regular. All of these faces have similar typographic design.
There are three different names that you can get from a Font object. The logical font name is simply the name that was used to construct the font. The font face name, or just font name for short, is the name of a particular font face, like Helvetica Bold. The family name is the name of the font family that determines the typographic design across several faces, like Helvetica.


Image class:
The abstract class Image is the superclass of all classes that represent graphical images. The image must be obtained in a platform-specific manner.

BufferedImage class:
The BufferedImage subclass describes an Image with an accessible buffer of image data. A BufferedImage is comprised of a ColorModel and a Raster of image data. The number and types of bands in the SampleModel of the Raster must match the number and types required by the ColorModel to represent its color and alpha components. All BufferedImage objects have an upper left corner coordinate of (0, 0). Any Raster used to construct a BufferedImage must therefore have minX=0 and minY=0.

VolatileImage class:
VolatileImage is an image which can lose its contents at any time due to circumstances beyond the control of the application (e.g., situations caused by the operating system or by other applications). Because of the potential for hardware acceleration, a VolatileImage object can have significant performance benefits on some platforms.
The drawing surface of an image (the memory where the image contents actually reside) can be lost or invalidated, causing the contents of that memory to go away. The drawing surface thus needs to be restored or recreated and the contents of that surface need to be re-rendered. VolatileImage provides an interface for allowing the user to detect these problems and fix them when they occur.

TexturePaint class:
The TexturePaint class provides a way to fill a Shape with a texture that is specified as a BufferedImage. The size of the BufferedImage object should be small because the BufferedImage data is copied by the TexturePaint object. At construction time, the texture is anchored to the upper left corner of a Rectangle2D that is specified in user space. Texture is computed for locations in the device space by conceptually replicating the specified Rectangle2D infinitely in all directions in user space and mapping the BufferedImage to each replicated Rectangle2D.


















