# Bevezető:
* https://docs.gtk.org/gtk4/overview.html
* https://docs.gtk.org/gdk4/class.Surface.html
* https://docs.gtk.org/gtk4/class.Application.html
* https://docs.gtk.org/gtk4/class.ApplicationWindow.html
* https://docs.gtk.org/gtk4/class.Frame.html
* https://docs.gtk.org/gtk4/class.Window.html

GTK depends on the following libraries:
-Cairo: Cairo is a 2D graphics library with support for multiple output devices.
-Pango: Pango is a library for internationalized text handling. It centers around the PangoLayout object, representing a paragraph of text. Pango provides the engine for GtkTextView, GtkLabel, GtkEntry, and all GTK widgets that display text.
-gdk-pixbuf: A small, portable library which allows you to create GdkPixbuf (“pixel buffer”) objects from image data or image files. You can use GdkPixbuf in combination with widgets like GtkImage to display images.
-graphene: A small library which provides vector and matrix datatypes and operations.

GTK is divided into three parts:
GDK: GDK is the abstraction layer that allows GTK to support multiple windowing systems. GDK provides window system facilities on Wayland, X11, Microsoft Windows, and Apple macOS.
GSK: GSK is an API for creating a scene graph from drawing operation, called “nodes”, and rendering it using different backends. GSK provides renderers for OpenGL, Vulkan and Cairo.
GTK: The GUI toolkit, containing UI elements, layout managers, data storage types for efficient use in GUI applications, and much more.


Gdk Surface class:
A GdkSurface is a rectangular region on the screen.
It’s a low-level object, used to implement high-level objects such as Gtk.Window or Gtk.Dialog in GTK.

Gtk Application class:
GtkApplication is a high-level API for writing applications.
It supports many aspects of writing a GTK application in a convenient fashion, without enforcing a one-size-fits-all model. Currently, GtkApplication handles GTK initialization, application uniqueness, session management, provides some basic scriptability and desktop shell 
integration by exporting actions and menus and manages a list of toplevel windows whose life-cycle is automatically tied to the life-cycle of your application. While GtkApplication works fine with plain GtkWindows, it is recommended to use it together with GtkApplicationWindow.

Gtk Window class:
A GtkWindow is a toplevel window which can contain other widgets.
Windows normally have decorations that are under the control of the windowing system and allow the user to manipulate the window (resize it, move it, close it,…).

Gtk ApplicationWindow class:
GtkApplicationWindow is a GtkWindow subclass that integrates with GtkApplication.
Notably, GtkApplicationWindow can handle an application menubar.
This class implements the GActionGroup and GActionMap interfaces, to let you add window-specific actions that will be exported by the associated GtkApplication, together with its application-wide actions. Window-specific actions are prefixed with the “win.” prefix and application-wide actions are prefixed with the “app.” prefix. Actions must be addressed with the prefixed name when referring to them from a GMenuModel.
Note that widgets that are placed inside a GtkApplicationWindow can also activate these actions, if they implement the GtkActionable interface.

Gtk Frame class:
GtkFrame is a widget that surrounds its child with a decorative frame and an optional label.
GtkFrame clips its child. You can use this to add rounded corners to widgets, but be aware that it also cuts off shadows.



# Drawing:
* https://docs.gtk.org/gtk4/drawing-model.html
* https://docs.gtk.org/gsk4/class.Renderer.html
* https://docs.gtk.org/gsk4/class.RenderNode.html
* https://docs.gtk.org/gdk4/class.DrawContext.html
* https://docs.gtk.org/gtk4/class.DrawingArea.html
* http://devernay.free.fr/cours/IHM/GTK/GGAD.pdf#page=163&zoom=100,128,609


The first step in “drawing” a window is that GTK creates render nodes for all the widgets in the window. The render nodes are combined into a tree that you can think of as a scene graph describing your window contents.

Render nodes belong to the GSK layer, and there are various kinds of them, for the various kinds of drawing primitives you are likely to need when translating widget content and CSS styling. Typical examples are text nodes, gradient nodes, texture nodes or clip nodes.

In the past, all drawing in GTK happened via cairo. It is still possible to use cairo for drawing your custom widget contents, by using a cairo render node.

A GSK renderer takes these render nodes, transforms them into rendering commands for the drawing API it targets, and arranges for the resulting drawing to be associated with the right surface. GSK has renderers for OpenGL, Vulkan and cairo.

Once you understand drawables, colors, visuals, graphics contexts, and fonts, actually drawing is very simple. This section is a quick summary of the GDK drawing routines. Remember that drawing is a server-side operation; for example, if you ask to draw a line, Xlib will send the line’s endpoints to the server, and the server will do the actual drawing using the specified GC (the GC is also a server-side resource).


Gsk Renderer class:
GskRenderer is a class that renders a scene graph defined via a tree of GskRenderNode instances.
Typically you will use a GskRenderer instance to repeatedly call gsk_renderer_render() to update the contents of its associated GdkSurface.

Gsk RenderNode class:
GskRenderNode is the basic block in a scene graph to be rendered using GskRenderer.
Each node has a parent, except the top-level node; each node may have children nodes.
Each node has an associated drawing surface, which has the size of the rectangle set when creating it.
Render nodes are meant to be transient; once they have been associated to a GskRenderer it’s safe to release any reference you have on them. 
All GskRenderNodes are immutable, you can only specify their properties during construction.

Gdk DrawContext class:
Base class for objects implementing different rendering methods.
GdkDrawContext is the base object used by contexts implementing different rendering methods, such as GdkCairoContext or GdkGLContext. 
It provides shared functionality between those contexts.
You will always interact with one of those subclasses.
A GdkDrawContext is always associated with a single toplevel surface.

Gdk Pixmap type:
A pixmap is an off-screen buffer you can draw graphics into. After drawing into a pixmap, you can copy it to a window, causing it to appear on the screen (when the window is visible). (You can also draw into a window directly, of course. Using a pixmap as a buffer allows you to rapidly update the screen without repeating a series of primitive drawing operations.) Pixmaps are also good to store image data loaded from disk, such as icons and logos. You can then copy the image to a window. In GDK, the pixmap type is called GdkPixmap. A pixmap with a single bit representing each pixel is called a bitmap; GDK’s bitmap type is GdkBitmap.

Gdk Drawable:
In X terminology, a drawable is anything you can draw graphics on. GDK has a corresponding type, called GdkDrawable. Drawables include windows, pixmaps, and bitmaps. GdkDrawable is used in function declarations when either a window or a pixmap is an acceptable argument. Functions that draw graphics take either type; functions that
move windows around or set window manager hints accept only windows.

Gtk DrawingArea class:
GtkDrawingArea is a widget that allows drawing with cairo. It’s essentially a blank widget; you can draw on it.




# Coloring:
* https://developer.gimp.org/api/2.0/gdk/gdk-Colormaps-and-Colors.html
* https://docs.gtk.org/gtk4/class.ColorChooserDialog.html
* https://docs.gtk.org/gtk4/class.ColorChooserWidget.html

GdkColor structure:
The GdkColor structure is used to describe an allocated or unallocated color.
guint32 pixel - For allocated colors, the value used to draw this color on the screen.
guint16 red - The red component of the color. This is a value between 0 and 65535, with 65535 indicating full intensitiy.
guint16 green - The blue component of the color.
guint16 blue - The green component of the color.

Gtk ColorChooserWidget class:
The GtkColorChooserWidget widget lets the user select a color.
By default, the chooser presents a predefined palette of colors, plus a small number of settable custom colors. It is also possible to select a different color with the single-color editor.
To enter the single-color editing mode, use the context menu of any color of the palette, or use the ‘+’ button to add a new custom color.
The chooser automatically remembers the last selection, as well as custom colors.

Gtk ColorChooserDialog class: 
A dialog for choosing a color. GtkColorChooserDialog implements the GtkColorChooser interface and does not provide much API of its own.



# Raster/vector
* http://devernay.free.fr/cours/IHM/GTK/GGAD.pdf#page=163&zoom=100,128,609

If you have a large number of objects, RGB mode can be faster than GDK mode.
Drawing to an RGB buffer is a simple matter of assigning to an array, which is much,
much faster than making a GDK call (since GDK has to contact the X server and ask
it to do the actual drawing). The expensive part is copying the RGB buffer to the
X server when you’re done. However, the copy takes the same amount of time no
matter how many canvas items you have, since it is done only once, when all the
items have been rendered.

This is a big win in an application called "Guppi" I’m in the process of writing. Guppi
is a plot program. One of the things it has to do is render a scatter plot with tens of
thousands of points. Each point is a small colored shape; if I called GDK to render
each, there would be tens of thousands of trips to the X server, possibly across a network. 
Instead, I use the canvas in RGB mode, with a custom canvas item representing
the scatter plot. This allows me to do all the rendering on the client side, and then the
canvas copies the RGB buffer to the server in a single burst. It’s quite fast and responsive. 
For less speed-intensive elements of the plot, such as the legend, I can save time
and use the built-in canvas items.

The one difficulty with direct-to-RGB rendering is that you need a rasterization library 
comparable to the GDK drawing primitives if you want to draw anything interesting. 
libart_lgpl is a very high-quality antialiased rasterization library, used by
the default canvas items. You can use it in your custom items as well, and it is the best
choice if you will only be drawing hundreds of shapes. If you’re drawing thousands
of shapes, however, you’ll quickly see the need for something faster. Fortunately,
this is available; the maintainers of a package called GNU Plotutils extracted the 
rasterization library from the X distribution, and during the development of Guppi I
extracted it from Plotutils and hacked it to work with the canvas’s RGB buffers. I also
added alpha transparency support. The resulting library allows you to draw on an
RGB buffer much as you would draw using GDK. The library is distributed under
the same license as the X Window System, and is free for anyone to include with their
application.



# Render:
* http://devernay.free.fr/cours/IHM/GTK/GGAD.pdf#page=163&zoom=100,128,609

The most important task of any canvas item is rendering itself onto the canvas.
Rendering is a two-stage process for efficiency reasons. The first stage, implemented 
in a GnomeCanvasItem’s update method, is guaranteed to happen only once per item
per rendering cycle; the idea is to do any expensive affine transformations or other 
calculations in the update method. In the second stage, the canvas item renders itself 
to some region on the screen. The render method implements stage two for antialiased 
items, while the draw method implements stage two for GDK items. An item’s render or 
draw method may be invoked multiple times during a canvas repaint.

Rendering occurs in a one-shot idle function. That is, whenever the canvas receives
an expose event or otherwise determines that a redraw is needed, it adds an idle
function which removes itself after a single invocation. (An idle function runs when
no GTK+ events are pending and the flow of execution is in the GTK+ main loop—see
the section called The Main Loop in Chapter 3 for details.) The canvas maintains a list
of redraw regions and adds to it whenever a redraw request is received, so it knows
which areas to repaint when the idle handler is finally invoked.

Canvas items carry a flag indicating whether they need to be updated. Whenever a
canvas item "changes" (for example, if you set a new fill color for GnomeCanvasRect),
it will call gnome_canvas_item_request_update() to set the "update needed" flag
for itself and the groups that contain it, up to and including the root canvas group.
(The GnomeCanvas widget is only aware of a single canvas item, the root group—all
other items are handled recursively when methods are invoked on the root group.) In
its one-shot idle function, the canvas invokes the update method of the root canvas
item if its update flag is set, then clears the flag so the update method will not be run
next time. The GnomeCanvasGroup update method does the same for each child
item.

Once all canvas items have been updated, the rendering process begins. The canvas
creates an RGB or GdkPixmap buffer, converts its list of redraw regions into a list of
buffer-sized rectangles, then invokes the render or draw method of the root canvas
group once per rectangle. After each rectangle is rendered, the buffer is copied to the
screen.



# Images and text:
* https://docs.gtk.org/gtk4/class.FontChooserWidget.html
* https://docs.gtk.org/gtk4/class.FontChooserDialog.html
* https://docs.gtk.org/Pango/class.FontFamily.html
* https://docs.gtk.org/Pango/class.Font.html
* https://docs.gtk.org/Pango/class.FontMap.html
* https://docs.gtk.org/Pango/class.Fontset.html
* https://docs.gtk.org/Pango/class.Coverage.html
* https://docs.gtk.org/gtk4/class.Image.html
* https://docs.gtk.org/gdk-pixbuf/class.Pixbuf.html
* https://docs.gtk.org/gdk-pixbuf/class.PixbufLoader.html


Pango Font class:
A PangoFont is used to represent a font in a rendering-system-independent manner.

Pango Coverage class:
A PangoCoverage structure is a map from Unicode characters to PangoCoverageLevel values.
It is often necessary in Pango to determine if a particular font can represent a particular character, and also how well it can represent that character. The PangoCoverage is a data structure that is used to represent that information. It is an opaque structure with no public fields.

Pango FontFamily class:
A PangoFontFamily is used to represent a family of related font faces.
The font faces in a family share a common design, but differ in slant, weight, width or other aspects.

Pango FontMap class:
A PangoFontMap represents the set of fonts available for a particular rendering system.
This is a virtual object with implementations being specific to particular rendering systems.

Pango Fontset class:
A PangoFontset represents a set of PangoFont to use when rendering text.
A PAngoFontset is the result of resolving a PangoFontDescription against a particular PangoContext. It has operations for finding the component font for a particular Unicode character, and for finding a composite set of metrics for the entire fontset.


Gtk FontChooserWidget class:
The GtkFontChooserWidget widget lets the user select a font. It is used in the GtkFontChooserDialog widget to provide a dialog for selecting fonts.

Gtk FontChooserDialog class:
The GtkFontChooserDialog widget is a dialog for selecting a font. GtkFontChooserDialog implements the GtkFontChooser interface and does not provide much API of its own.


GdkPixbuf Pixbuf class:
A pixel buffer. GdkPixbuf contains information about an image’s pixel data, its color space, bits per sample, width and height, and the rowstride (the number of bytes between the start of one row and the start of the next). Image data in a pixbuf is stored in memory in an uncompressed, packed format. Rows in the image are stored top to bottom, and in each row pixels are stored from left to right.
There may be padding at the end of a row.

GdkPixbuf PixbufLoader class:
Incremental image loader.
GdkPixbufLoader provides a way for applications to drive the process of loading an image, by letting them send the image data directly to the loader instead of having the loader read the data from a file. Applications can use this functionality instead of gdk_pixbuf_new_from_file() or gdk_pixbuf_animation_new_from_file() when they need to parse image data in small chunks. For example, it should be used when reading an image from a (potentially) slow network connection, or when loading an extremely large file.

Gtk Image class:
The GtkImage widget displays an image. Various kinds of object can be displayed as an image; most typically, you would load a GdkTexture from a file, using the convenience function gtk_image_new_from_file(). If the file isn’t loaded successfully, the image will contain a “broken image” icon similar to that used in many web browsers.









